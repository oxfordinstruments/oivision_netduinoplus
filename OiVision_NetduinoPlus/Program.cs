﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using System.Threading;
using ElzeKool.io;
using ElzeKool.io.sht11_io;
using MCP320XTest;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;
using Microsoft.SPOT.Net.NetworkInformation;
using RawSocketPingClass;
using SecretLabs.NETMF.Hardware.NetduinoPlus;
using SerialPortLearn;
using Microsoft.SPOT.IO;
using SecretLabs.NETMF.Hardware;
using Toolbox.NETMF.NET;

using System.Net;
using System.Collections;
using Json.NETMF;


#if DEBUG
namespace System.Diagnostics
{
    public enum DebuggerBrowsableState
    {
        Never,
        Collapsed,
        RootHidden
    }
}
#warning Debugging Enabled
#endif
namespace OiVision_NetduinoPlus
{
    public class Program
    {
        const string Rev = "1.7"; //Board Revision for this software
        //bool mcp3204inuse = false; //mcp320x chip
        bool pingable = true; //change for networks that do not allow pinging outside 9-22-11
        public double Tc_Offset = -39.7;  //Any + or - number e.g. -10.223555  This is set using serial console
        public double Rh_offset = 0.0;  //Any + or - number e.g. 10.223555  This is set using serial console
        string Site_Name = "";
        //Cpu.Pin SHT1x_Data_Pin = Pins.GPIO_PIN_A0;
        //Cpu.Pin SHT1x_Clock_Pin = Pins.GPIO_PIN_A1;
        //const string Server = "69.27.55.2"; //OI Vision Web Server IP Address (must be an ip) //Old 8-17-11
        int DelayM = 30; //Minutes delay to send reading. this is rx from the web server
        string version = "";
        const double HeThreshold = 0.10;  // Value in V DC
        const double ADCRefVoltage = 5050;
        bool FieldOnDetect = true;
        bool CompressorDetect = true;
        double Rh = 0;
        double Tc = 0;
        double Tf = 0;
        int Tz = 0; //Time Zone
        bool FirstRun = true;
        double He_Offset = 0;
        double HeP_Offset = 0;
        double Wf_Offset = 0;
        double Wtf_Offset = 0;
        double He = 111.11;
        double[] HeX = new double[4] { 0, 0, 0, 0 };
        double HeP = 0;
        double[] HePX = new double[4] { 0, 0, 0, 0 };
        double Wf = 0;
        double[] WfX = new double[4] { 0, 0, 0, 0 };
        double Wtc = 0;
        double Wtf = 0;
        double[] WtfX = new double[4] { 0, 0, 0, 0 };
        bool FieldOn = false;
        bool CompressorOn = false;
        MCP320X mcp320xA = new MCP320X(2, Pins.GPIO_PIN_D9);
        MCP320X mcp320xB = new MCP320X(2, Pins.GPIO_PIN_D10);
        MCP320X mcp3204 = new MCP320X(4, Pins.GPIO_PIN_D9);
        ushort adc12bit;
        double convValue = 0.0;
        int Scroll = 2000;
        const int LCDBrightness = 255;
        const string defaultserver = "oi-vision.net";//"67.199.146.103";
        String Server = "oi-vision.net";//"67.199.146.103"; //OI Vision Web Server IP Address
        DateTime d1; // Used for web server updates of readings
        DateTime d2; // Used for web server updates of readings. Next upload time
        private SHT11_GPIO_IOProvider SHT1x_IO;
        private SensirionSHT11 SHT1x;
        LXIT.Hardware.SerLCD LCD = new LXIT.Hardware.SerLCD("COM1");
        InputPort ProgPin = new InputPort(Pins.GPIO_PIN_D4, true, Port.ResistorMode.PullUp); //Ground pin to activate serial console
        InputPort FieldOnPin = new InputPort(Pins.GPIO_PIN_D5, true, Port.ResistorMode.Disabled); //Change accordingly for FieldOn Switch
        InputPort CompressorOnPin = new InputPort(Pins.GPIO_PIN_D6, true, Port.ResistorMode.Disabled); //Change Accordingly for Compressor On
        NetworkInterface networkInterface = NetworkInterface.GetAllNetworkInterfaces()[0];
        SerialPortHelper PCport = new SerialPortHelper(SerialPorts.COM2);
        OutputPort MLED = new OutputPort(Pins.GPIO_PIN_A5, true);
        OutputPort WD = new OutputPort(Pins.GPIO_PIN_D7, false);
        //OutputPort WD0 = new OutputPort(Pins.GPIO_PIN_D8, true);
        //OutputPort WD2 = new OutputPort(Pins.GPIO_PIN_A3, false);
        public Thread _GetMeasurments;
        public Thread _RebootWatch;
        public Thread _WatchDogConfig;
        bool testmode = false;
        public string mac;
        public static Thread _Run;

        public static void Main()
        {
            var MainProg = new Program();

            _Run = new Thread(MainProg.Run);

            MainProg.MAC();

            Assembly asm = Assembly.GetExecutingAssembly();
            AssemblyName assemblyName = asm.GetName();
            MainProg.version = assemblyName.Version.ToString();

            Debug.Print("Starting Main Program");
            _Run.Start();

            // Create IO provider for temperature sensor
            MainProg.SHT1x_IO = new SHT11_GPIO_IOProvider(Pins.GPIO_PIN_A0, Pins.GPIO_PIN_A1);

            // Create SHT11 Interface with the IO provider we've just created
            MainProg.SHT1x = new SensirionSHT11(MainProg.SHT1x_IO);

        }
        public void Run()
        {
            Testing(); //JRD testing
                        
            LCD.Open();
            if (testmode != true)
            {
                Setup();
            }
            d1 = DateTime.Now;
            d2 = DateTime.Now.AddMinutes(DelayM);

            Debug.Print("Starting Reboot Watch");
            _RebootWatch = new Thread(delegate () { RebootWatch(); });
            _RebootWatch.Start();

            Debug.Print("Starting Get Measurements");
            _GetMeasurments = new Thread(delegate () { GetMeasurements(); });
            _GetMeasurments.Start();

            while (true)
            {
                WD.Write(true);
                Thread.Sleep(10);
                WD.Write(false);
                if (_GetMeasurments.IsAlive == false)
                {
                    _GetMeasurments.Start();
                }
                d1 = DateTime.Now;
                MeasureRoomTemperatureAndRh();
                CheckMagnetOutputs();
                if (testmode == true)
                {
                    LCD.ClearDisplay();
                    LCD.Write("Test Mode");
                    Thread.Sleep(Scroll);
                }
                WD.Write(true);
                Thread.Sleep(10);
                WD.Write(false);
                LCD.ClearDisplay();
                LCD.Write("DHCP: " + networkInterface.IsDhcpEnabled.ToString());
                LCD.SetCursor(1, 2);
                LCD.Write("IP: " + networkInterface.IPAddress.ToString());
                Thread.Sleep(Scroll);
                LCD.ClearDisplay();
                LCD.Write("Room Temperature");
                LCD.SetCursor(1, 2);
                LCD.Write(Tc.ToString("F") + " ");
                LCD.Write(0);
                LCD.Write("  " + Tf.ToString("F") + " ");
                LCD.Write(1);
                Thread.Sleep(Scroll);
                LCD.ClearDisplay();
                LCD.Write("Room Humidity");
                LCD.SetCursor(1, 2);
                LCD.Write(Rh.ToString("F") + " %RH");
                Thread.Sleep(Scroll);
                WD.Write(true);
                Thread.Sleep(10);
                WD.Write(false);
                LCD.ClearDisplay();
                LCD.Write("He Level");
                LCD.SetCursor(1, 2);
                LCD.Write(He.ToString("F") + " %");
                Thread.Sleep(Scroll);
                LCD.ClearDisplay();
                LCD.Write("He Pressure");
                LCD.SetCursor(1, 2);
                LCD.Write(HeP.ToString("F") + " PSIG");
                Thread.Sleep(Scroll);
                LCD.ClearDisplay();
                LCD.Write("Water Flow");
                LCD.SetCursor(1, 2);
                LCD.Write(Wf.ToString("F") + " GPM");
                Thread.Sleep(Scroll);
                WD.Write(true);
                Thread.Sleep(10);
                WD.Write(false);
                LCD.ClearDisplay();
                LCD.Write("Water Temp.");
                LCD.SetCursor(1, 2);
                LCD.Write(Wtc.ToString("F"));
                LCD.Write(0);
                LCD.Write("  " + Wtf.ToString("F"));
                LCD.Write(1);
                Thread.Sleep(Scroll);
                LCD.ClearDisplay();
                LCD.Write(DateTime.Now.ToString("MMMM d, yyyy"));
                LCD.SetCursor(1, 2);
                LCD.Write(DateTime.Now.ToString("h:mm:ss t"));
                Thread.Sleep(Scroll);
                LCD.ClearDisplay();
                LCD.Write("Next Update");
                LCD.SetCursor(1, 2);
                LCD.Write(d2.ToString("h:mm:ss t"));
                Thread.Sleep(Scroll);
                // Add more here

                LCD.ClearDisplay();

                if (FieldOnDetect == true)
                {
                    if (FieldOn == true)
                    {
                        LCD.Write("Magnet Field: ON");
                    }
                    else
                    {
                        LCD.Write("Magnet Field: OFF !");
                    }
                }
                else
                {
                    LCD.Write("Magnet Field: ON*");
                }

                LCD.SetCursor(1, 2);
                if (CompressorOn)
                {
                    if (CompressorDetect == true)
                    {
                        LCD.Write("Compressor: ON");
                    }
                    else
                    {
                        LCD.Write("Compressor: ON*");
                    }
                }
                else
                {
                    LCD.Write("Compressor: OFF");
                }
                Thread.Sleep(Scroll);
                WD.Write(true);
                Thread.Sleep(10);
                WD.Write(false);
                Debug.Print("");
                Debug.Print("Checking Dates");
                Debug.Print("Current DT = " + d1);
                Debug.Print("Waiting for " + d2);
                if (d1 >= d2 || FirstRun == true)
                {
                    LCD.ClearDisplay();
                    LCD.MoveHome();
                    LCD.Write("Sending Data...");
                    LCD.SetCursor(1, 2);
                    LCD.Write(d1.ToString("h:mm:ss t"));
                    //_GetMeasurments.Suspend();
                    if (Send())
                    {
                        WD.Write(true);
                        Thread.Sleep(10);
                        WD.Write(false);
                        LCD.ClearDisplay();
                        LCD.MoveHome();
                        LCD.Write("Data Sent");
                        LCD.SetCursor(1, 2);
                        LCD.Write(d1.ToString("h:mm:ss t"));
                        Thread.Sleep(1000);
                        FirstRun = false;
                    }
                    else
                    {
                        WD.Write(true);
                        Thread.Sleep(10);
                        WD.Write(false);
                        LCD.ClearDisplay();
                        LCD.MoveHome();
                        LCD.Write("Error Sending Data");
                        LCD.SetCursor(1, 2);
                        LCD.Write("Call Service");
                        Thread.Sleep(5000);
                        FirstRun = true;
                    }
                    //if (_GetMeasurments.ThreadState == ThreadState.Suspended || _GetMeasurments.ThreadState == ThreadState.SuspendRequested)
                    //{
                    //    _GetMeasurments.Resume();
                    //}
                }

            }

        }
        private void RebootWatch()
        {
            //Debug.Print("Watch Started");
            Thread.Sleep(30000);
            //Utility.SetLocalTime(new DateTime(2000,1,1,0,0,0,0));
            DateTime d1 = DateTime.Now;
            Random r1 = new Random();
            int r2 = r1.Next(16);
            while (true)
            {
                Debug.Print("<<<<<<<<<<<<<<<<<<< Uptime >>>>>>>>>>>>>>>>>>>>" + PowerState.Uptime.ToString());
                //Debug.Print("Current Date: " + DateTime.Now.ToString());
                //Debug.Print("Random Min/Sec: " + r2);

                Thread.Sleep(10000);
                d1 = DateTime.Now;
                if (PowerState.Uptime.Hours >= 1 && PowerState.Uptime.Seconds >= 1 && d1.Hour == 06 && d1.Minute >= r2 && d1.Second >= r2)
                {
                    Debug.Print("Reboot !!!!!!");
                    //Debug.Print(DateTime.Now.ToString());
                    //Debug.Print(r2.ToString());
                    Microsoft.SPOT.Hardware.PowerState.RebootDevice(false, 3000);
                }
                if (PowerState.Uptime.Days >= 1 && PowerState.Uptime.Seconds >= 1 && d1.Hour == 06 && d1.Minute >= r2 && d1.Second >= r2)
                {
                    Debug.Print("Reboot !!!!!!");
                    //Debug.Print(DateTime.Now.ToString());
                    //Debug.Print(r2.ToString());
                    Microsoft.SPOT.Hardware.PowerState.RebootDevice(false, 3000);
                }
                if (PowerState.Uptime.Days >= 2)
                {
                    Microsoft.SPOT.Hardware.PowerState.RebootDevice(false, 3000);
                }

            }
        }
        private void WatchDogConfig()
        {
            while (true)
            {
                WD.Write(true);
                Thread.Sleep(10);
                WD.Write(false);
                Thread.Sleep(500);
            }
        }
        public bool HelloSettings()
        {
            if (HelloSettings2("/monitor/mmHello.php?mac=" + MAC() + "&ver=" + version))
            {
                return true;
            }
            Thread.Sleep(2000);
            Debug.Print("HELLO Trying Again");
            if (HelloSettings2("/monitor/mmHello.php?mac=" + MAC() + "&ver=" + version))
            {
                return true;
            }
            Thread.Sleep(2000);
            Debug.Print("HELLO Trying Again");
            if (HelloSettings2("/monitor/mmHello.php?mac=" + MAC() + "&ver=" + version))
            {
                return true;
            }
            Thread.Sleep(2000);
            Debug.Print("HELLO Trying Again");
            if (HelloSettings2("/monitor/mmHello.php?mac=" + MAC() + "&ver=" + version))
            {
                return true;
            }
            else
            {
                return false;
            }


        }
        public bool HelloSettings2(string url) //Gets a hello reply and settings from the server
        {
            Debug.Print("<<<< HELLO >>>>");
            //var url = "http://" + Server + "/monitor/mmHello.php?mac=" + MAC() + "&ver=" + version;
            //var url = "/monitor/mmHello.php?mac=" + MAC() + "&ver=" + version; 
            //var ip = ""; //must be an IP address
            //string reply = UrlRetriever.GetWebPage(ip, url, 80);
            string reply = GetWebPage(url, 80, Server);
            Debug.Print(reply); //Should reply with Hello and settings
            if (reply == "::nothing came back::")
            {
                Debug.Print("GET WEBPAGE ERROR");
                Debug.Print("<<<< HELLO End >>>>");
                return false;
            }
            int s;
            int e;
            s = reply.IndexOf("::");
            e = reply.IndexOf("::", (s + 1));
            DelayM = int.Parse(reply.Substring((s + 2), (e - s) - 2).ToString());
            Debug.Print("Update interval: " + DelayM.ToString());
            s = e;
            e = reply.IndexOf("::", (s + 1));
            Tz = int.Parse(reply.Substring((s + 2), (e - s) - 2).ToString());
            Debug.Print("Time Zone Offset: " + Tz.ToString());
            s = e;
            e = reply.IndexOf("::", (s + 1));
            if (reply.Substring((s + 2), (e - s) - 2).ToLower() == MAC().ToLower())
            {
                Debug.Print("MAC: " + reply.Substring((s + 2), (e - s) - 2).ToString());

                s = e;
                e = reply.IndexOf("::", (s + 1));
                Site_Name = reply.Substring((s + 2), (e - s) - 2).ToString();
                Debug.Print("Site Name: " + Site_Name);
            }
            else
            {
                Debug.Print("<<<< HELLO End >>>>");
                return false;
            }
            s = e;
            e = reply.IndexOf("::", (s + 1));
            if (reply.Substring((s + 2), (e - s) - 2).ToLower() == "y")
            {
                Debug.Print("Enabled: " + reply.Substring((s + 2), (e - s) - 2).ToString());
                Debug.Print("<<<< HELLO End>>>>");
                return true;
            }
            else
            {
                Debug.Print("Enabled: " + reply.Substring((s + 2), (e - s) - 2).ToString());
                Debug.Print("<<<< HELLO End >>>>");
                return false;
            }

        }
        public bool Setup()
        {
            WD.Write(true);
            Thread.Sleep(10);
            WD.Write(false);
            LCD.Write(new byte[] { 0xFE, 0x40 });
            //LCD.Write("                                        "); //Clears LCD Startup Screen
            LCD.Write("     OiVision        Oxford Instruments "); //Clears LCD Startup Screen
            Thread.Sleep(100);
            LCD.Write(new byte[] { 0xFE, 0x4E, 0x00, 0x08, 0x14, 0x08, 0x03, 0x04, 0x04, 0x03, 0x00 }); //Degree C symbol
            Thread.Sleep(100);
            LCD.Write(new byte[] { 0xFE, 0x4E, 0x01, 0x08, 0x14, 0x08, 0x07, 0x04, 0x06, 0x04, 0x00 }); //Degree F symbol
            Thread.Sleep(100);
            //LCD.Write(new byte[] { 0xFE, 0x4E, 0x02, 0x06, 0x09, 0x09, 0x06, 0x00, 0x00, 0x00, 0x00 }); //Degree symbol
            //Thread.Sleep(100);
            //LCD.Write(new byte[] { 0xFE, 0x4E, 0x03, 0x00, 0x04, 0x0E, 0x1B, 0x1B, 0x0E, 0x04, 0x00 }); //Loading symbol
            //Thread.Sleep(100);
            LCD.ClearDisplay();
            //LCD.SetBrightness(LCDBrightness);
            LCD.SetCursor(1, 1);
            LCD.Write("OiVision");
            LCD.SetCursor(1, 2);
            version = version + "_" + Rev;
            LCD.Write("Ver: " + version);
            Thread.Sleep(2000);
            LCD.ClearDisplay();
            LCD.MoveHome();
            LCD.Write("Starting Up...");
            Thread.Sleep(1000);
            LCD.SetCursor(1, 2);
            LCD.Write("SHT Init");
            Thread.Sleep(300);
            // Soft-Reset the SHT11
            if (SHT1x.SoftReset())
            {
                // Softreset returns True on error
                LCD.ClearDisplay();
                LCD.Write("SHT Start Error");
                Thread.Sleep(10000);
                Microsoft.SPOT.Hardware.PowerState.RebootDevice(false, 3000);
                throw new Exception("Error while resetting SHT11");
            }
            WD.Write(true);
            Thread.Sleep(10);
            WD.Write(false);
            // Set Temperature and Humidity to less acurate 12/8 bit
            if (SHT1x.WriteStatusRegister(SensirionSHT11.SHT11Settings.LessAcurate))
            {
                // WriteRegister returns True on error
                LCD.ClearDisplay();
                LCD.Write("SHT 12 Bit Error");
                Thread.Sleep(10000);
                Microsoft.SPOT.Hardware.PowerState.RebootDevice(false, 3000);
                throw new Exception("Error while writing status register SHT11");
            }
            LCD.SetCursor(1, 2);
            LCD.Write("SHT Check   ");
            MeasureRoomTemperatureAndRh();
            Thread.Sleep(500);
            LCD.SetCursor(1, 2);
            LCD.Write("CFG Load   ");
            try
            {
                if (Load_CFG() != true)
                {
                    if (ProgPin.Read() != false)
                    {
                        throw new Exception("Error loading config file");
                    }
                    else
                    {
                        Config();
                    }
                }
            }
            catch (Exception e)
            {
                Debug.Print(e.ToString());
                LCD.ClearDisplay();
                LCD.Write("CFG Load Error");
                LCD.SetCursor(1, 2);
                LCD.Write("Call Service");
                Thread.Sleep(5000);
                Microsoft.SPOT.Hardware.PowerState.RebootDevice(false, 3000);
                throw new Exception("Error loading config file");
            }
            WD.Write(true);
            Thread.Sleep(10);
            WD.Write(false);
            Thread.Sleep(500);
            LCD.SetCursor(1, 2);
            LCD.Write("DHCP: " + networkInterface.IsDhcpEnabled.ToString());
            Thread.Sleep(1000);
            LCD.SetCursor(1, 2);
            LCD.Write("IP: " + networkInterface.IPAddress.ToString());
            Thread.Sleep(1000);
            if (ProgPin.Read() == false)
            {
                Config();
            }
            LCD.SetCursor(1, 2);
            LCD.Write("Checking Network    ");
            Thread.Sleep(500);
            WD.Write(true);
            Thread.Sleep(10);
            WD.Write(false);
            try
            {
                if (Ping())
                {
                    LCD.SetCursor(1, 2);
                    LCD.Write("Network Available   ");
                    Thread.Sleep(1500);
                }
                else
                {
                    if (networkInterface.IsDhcpEnabled)
                    {
                        Debug.Print("Renewing DHCP Lease");
                        networkInterface.RenewDhcpLease();
                        if (Ping())
                        {
                            LCD.SetCursor(1, 2);
                            LCD.Write("Network Available   ");
                            Thread.Sleep(1500);
                        }
                        else
                        {
                            LCD.SetCursor(1, 2);
                            LCD.Write("Network Unavailable ");
                            Thread.Sleep(1500);
                            throw new Exception("Connect Error");
                        }
                    }
                    else
                    {
                        LCD.SetCursor(1, 2);
                        LCD.Write("Network Unavailable ");
                        Thread.Sleep(1500);
                        throw new Exception("Connect Error");
                    }
                }
            }
            catch (Exception e)
            {
                Debug.Print(e.ToString());
                LCD.ClearDisplay();
                LCD.Write("Connect Error");
                LCD.SetCursor(1, 2);
                LCD.Write("Check Network");
                Thread.Sleep(10000);
                Microsoft.SPOT.Hardware.PowerState.RebootDevice(false, 3000);
                throw new Exception("Connect Error");
            }
            WD.Write(true);
            Thread.Sleep(10);
            WD.Write(false);
            Thread.Sleep(500);
            LCD.SetCursor(1, 2);
            LCD.Write("HELLO Load          ");
            try
            {
                if (HelloSettings())
                {
                    Debug.Print("Rx a valid reply from the OI Vision Server");
                }
                else
                {
                    //Debug.Print("Rx a bad reply from the OI Vision Server");
                    LCD.ClearDisplay();
                    LCD.Write("HELLO Not Valid");
                    LCD.SetCursor(1, 2);
                    //LCD.Write("Call Support");
                    LCD.Write(MAC());
                    int z = 0;
                    while (z < 20)
                    {
                        Thread.Sleep(60000);
                        z = z + 1;
                    }
                    Microsoft.SPOT.Hardware.PowerState.RebootDevice(false, 3000);
                }

            }
            catch (Exception e)
            {
                Debug.Print(e.ToString());
                LCD.ClearDisplay();
                LCD.Write("HELLO Error");
                LCD.SetCursor(1, 2);
                //LCD.Write("Call Support");
                LCD.Write(MAC());
                Thread.Sleep(10000);
                Microsoft.SPOT.Hardware.PowerState.RebootDevice(false, 3000);
                throw new Exception("HELLO Error");
            }
            Thread.Sleep(500);
            WD.Write(true);
            Thread.Sleep(10);
            WD.Write(false);
            LCD.SetCursor(1, 2);
            LCD.Write("NTP Load            ");
            try
            {
                if (GetServerTime(Tz))
                {
                    LCD.SetCursor(1, 2);
                    LCD.Write(DateTime.Now.ToString("g"));
                    Thread.Sleep(1500);
                }
                else
                {
                    LCD.SetCursor(1, 2);
                    LCD.Write("Connect Failed");
                    Thread.Sleep(1500);
                    throw new Exception("Connect Error");
                }
                Debug.Print("Current System Time: " + DateTime.Now.ToString());
            }
            catch (Exception e)
            {
                Debug.Print(e.ToString());
                LCD.ClearDisplay();
                LCD.Write("NTP Error");
                LCD.SetCursor(1, 2);
                LCD.Write("Call Support");
                Thread.Sleep(10000);
                Microsoft.SPOT.Hardware.PowerState.RebootDevice(false, 3000);
                throw new Exception("NTP Error");
            }
            WD.Write(true);
            Thread.Sleep(10);
            WD.Write(false);
            LCD.SetCursor(1, 2);
            LCD.Write("MAC Check           ");
            try
            {
                if (CheckMAC())
                {
                    Debug.Print("MAC is valid.");
                }
                else
                {
                    LCD.ClearDisplay();
                    LCD.Write("MAC Check Failed");
                    LCD.SetCursor(1, 2);
                    LCD.Write("Call Service");
                    Thread.Sleep(60000);
                    Thread.Sleep(60000);
                    Microsoft.SPOT.Hardware.PowerState.RebootDevice(false, 3000);
                }
            }
            catch (Exception e)
            {
                Debug.Print(e.ToString());
                LCD.ClearDisplay();
                LCD.Write("MAC Check Error");
                LCD.SetCursor(1, 2);
                LCD.Write("Call Support");
                Thread.Sleep(10000);
                Microsoft.SPOT.Hardware.PowerState.RebootDevice(false, 3000);
                throw new Exception("MAC Check Error");
            }
            return true;

        }
        private void MeasureRoomTemperatureAndRh()
        {
            SHT1x.readTemp = 0.0;
            // Read Temperature with SHT11 VDD = +/- 3.5V and in Celcius
            var temperature = SHT1x.ReadTemperature(SensirionSHT11.SHT11VDD_Voltages.VDD_3_5V,
                SensirionSHT11.SHT11TemperatureUnits.Celcius);
            temperature = temperature + Tc_Offset;
            //Debug.Print("Room Temp C Reading Actual: " + temperature);
            double f = ((temperature * 9) / 5) + 32;
            //Debug.Print("Tempature Fahrenheit: " + f.ToString());

            // Read Humidity with SHT11 VDD = +/- 3.5V 
            var humiditiy = SHT1x.ReadRelativeHumidity(SensirionSHT11.SHT11VDD_Voltages.VDD_3_5V);
            humiditiy = humiditiy + Rh_offset;
            //Debug.Print("Room Humiditiy Reading Actual: " + humiditiy);

            Tc = temperature;
            Tf = f;
            Rh = humiditiy;


        }
        private void GetMeasurements()
        {
            //Debug.Print("Starting Measurment Thread");
            while (true)
            {
                MLED.Write(true);
                //Water Flow
                adc12bit = mcp320xA.ReadADC(0, true, 50);
                //Debug.Print("Water Flow ADC Value: " + adc12bit);
                // convert from 12 bit value to V reading... assume we're using 5.0V as the supply voltage.
                convValue = (ADCRefVoltage * adc12bit / 4096) / 1000;
                //Debug.Print("Water Flow (Vdc): " + convValue);
                //Polynomial math goes here.  example: 5v=100gpm and 1v=0gpm
                convValue = (convValue * 0.7511274019160) + 0.6031050243629;
                //Debug.Print("Water Flow Reading Actual: " + convValue);
                if (FirstRun)
                {
                    WfX[0] = convValue + Wf_Offset;
                    WfX[1] = convValue + Wf_Offset;
                    WfX[2] = convValue + Wf_Offset;
                    WfX[3] = convValue + Wf_Offset;
                    if ((convValue + Wf_Offset) >= 0.90)
                    {
                        Wf = convValue + Wf_Offset;
                    }
                    else
                    {
                        Wf = 0.0;
                    }
                }
                else
                {
                    WfX[0] = WfX[1];
                    WfX[1] = WfX[2];
                    WfX[2] = WfX[3];
                    WfX[3] = convValue + Wf_Offset;
                    if ((convValue + Wf_Offset) >= 0.90)
                    {
                        Wf = (WfX[0] + WfX[1] + WfX[2] + WfX[3]) / 4;
                    }
                    else
                    {
                        Wf = 0.0;
                    }

                }
                Debug.Print("Water Flow Display: " + Wf.ToString("F"));

                //Water Temp
                adc12bit = mcp320xA.ReadADC(1, true, 50);
                //Debug.Print("Water Temp ADC Value: " + adc12bit);
                // convert from 12 bit value to V reading... assume we're using 5.0V as the supply voltage.
                convValue = (ADCRefVoltage * adc12bit / 4096) / 1000;
                //Debug.Print("Water Temp (Vdc): " + convValue);
                //Polynomial math goes here.  example: 5v=100 deg c and 1v=0 deg c
                //convValue = (convValue * 25.4423) - 25.1447;//old poly
                convValue = (convValue * 20.08413234737) + 0.1576700508843;
                //Debug.Print("Water Temp F Reading Actual: " + convValue);
                if (FirstRun)
                {
                    WtfX[0] = convValue + Wtf_Offset;
                    WtfX[1] = convValue + Wtf_Offset;
                    WtfX[2] = convValue + Wtf_Offset;
                    WtfX[3] = convValue + Wtf_Offset;
                    Wtf = convValue + Wtf_Offset;
                }
                else
                {
                    WtfX[0] = WtfX[1];
                    WtfX[1] = WtfX[2];
                    WtfX[2] = WtfX[3];
                    WtfX[3] = convValue + Wtf_Offset;
                    Wtf = (WtfX[0] + WtfX[1] + WtfX[2] + WtfX[3]) / 4;
                }
                Wtc = ((Wtf - 32) * 5) / 9;

                Debug.Print("Water Temp F Display: " + Wtf.ToString("F"));
                //Debug.Print("Water Temp C Display: " + Wtc.ToString("F"));

                //He Pressure
                adc12bit = mcp320xB.ReadADC(1, true, 50);
                //Debug.Print("He Pressure ADC Value: " + adc12bit);
                // convert from 12 bit value to V reading... assume we're using 5.0V as the supply voltage.
                convValue = (ADCRefVoltage * adc12bit / 4096) / 1000;
                //Debug.Print("He Pressure(Vdc): " + convValue);
                //Polynomial math goes here.  example: 5v=100 deg c and 1v=0 deg c
                //convValue = (convValue * 3.636523949299) + -3.596646745677;
                convValue = (convValue * 3.627402531871) + -3.633238886333;
                //Debug.Print("He Pressure Reading Actual: " + convValue);
                if (FirstRun)
                {
                    HePX[0] = convValue + HeP_Offset;
                    HePX[1] = convValue + HeP_Offset;
                    HePX[2] = convValue + HeP_Offset;
                    HePX[3] = convValue + HeP_Offset;
                    HeP = convValue + HeP_Offset;
                }
                else
                {
                    HePX[0] = HePX[1];
                    HePX[1] = HePX[2];
                    HePX[2] = HePX[3];
                    HePX[3] = convValue + HeP_Offset;
                    HeP = (HePX[0] + HePX[1] + HePX[2] + HePX[3]) / 4;
                }
                Debug.Print("He Pressure Display: " + HeP.ToString("F"));

                //He %
                adc12bit = mcp320xB.ReadADC(0, true, 100);
                //Debug.Print("He Level ADC Value: " + adc12bit);
                // convert from 12 bit value to V reading... assume we're using 5.0V as the supply voltage.
                convValue = (ADCRefVoltage * adc12bit / 4096) / 1000;
                //Debug.Print("He Level (Vdc): " + convValue);
                if (convValue >= HeThreshold)
                {
                    //Polynomial math goes here.  example: 5v=0% and 1v=100%
                    //convValue = (((convValue * 100) * -0.145849) + 99.94723); // old poly
                    //convValue = (convValue * -27.19246423233) + 99.97433368139; //old poly kinda works
                    convValue = (convValue * -21.08302807570) + 99.95558514737;
                    Debug.Print("He Reading Actual: " + (convValue - He_Offset) + "  -Updated");
                    if (FirstRun)
                    {
                        HeX[0] = convValue + He_Offset;
                        HeX[1] = convValue + He_Offset;
                        He = convValue + He_Offset;
                    }
                    else
                    {
                        HeX[0] = HeX[1];
                        HeX[1] = convValue + He_Offset;
                        He = (HeX[0] + HeX[1]) / 2;
                    }
                    //He = convValue + He_Offset;
                    //He_Save();
                }
                else
                {
                    Debug.Print("He Reading Actual: " + (convValue + He_Offset));
                }
                //Debug.Print("He Reading Display: " + He.ToString("F"));
                MLED.Write(false);
                Thread.Sleep(300);
                MLED.Write(true);
                Thread.Sleep(200);
                MLED.Write(false);
                Thread.Sleep(1000);
                Debug.Print("");
            }
        }
        private void CheckMagnetOutputs()
        {
            if (FieldOnDetect == true)
            {
                FieldOn = FieldOnPin.Read();
            }
            else
            {
                FieldOn = true;
            }
            if (CompressorDetect == true)
            {
                CompressorOn = CompressorOnPin.Read();
            }
            else
            {
                CompressorOn = true;
            }
            Debug.Print("FieldOn Detected: " + FieldOn.ToString() + " (Raw input): " + FieldOnPin.Read().ToString());
            Debug.Print("Compressor On: " + CompressorOn.ToString() + " (Raw input): " + CompressorOnPin.Read().ToString());
        }
        private void Config()
        {
            Debug.Print("Config");
            _WatchDogConfig = new Thread(delegate () { WatchDogConfig(); });
            if (!_WatchDogConfig.IsAlive)
            {
                _WatchDogConfig.Start();
                Debug.Print("WatchDogConfig Start");
            }

        Config:
            LCD.ClearDisplay();
            LCD.Write("     Config     ");
            PCport.PrintClear();
            PCport.PrintLine("OI Vision");
            PCport.PrintLine("Version: " + version);
            PCport.PrintLine("Board Revision: " + Rev);
            PCport.PrintLine("");
            PCport.PrintLine("Server: " + Server);
            PCport.PrintLine("IP: " + networkInterface.IPAddress);
            PCport.PrintLine("Subnet: " + networkInterface.SubnetMask);
            PCport.PrintLine("Gateway: " + networkInterface.GatewayAddress);
            try
            {
                PCport.PrintLine("DNS Pri: " + networkInterface.DnsAddresses[0]);
                PCport.PrintLine("DNS Sec: " + networkInterface.DnsAddresses[1]);
            }
            catch (Exception e)
            {
                Debug.Print("DNS Get Error");
                Debug.Print(e.ToString());
            }
            PCport.PrintLine("DHCP: " + networkInterface.IsDhcpEnabled);
            PCport.PrintLine("MAC: " + MAC());
            PCport.PrintLine("Timezone: -" + Tz);
            PCport.PrintLine("");
            PCport.PrintLine("");
            PCport.PrintLine("1 - Change Network Settings");
            PCport.PrintLine("2 - Enable/Disable DHCP");
            PCport.PrintLine("3 - Change Server");
            PCport.PrintLine("4 - Save Config");
            PCport.PrintLine("5 - Restore Factory Defaults");
            PCport.PrintLine("6 - Advanced");
            PCport.PrintLine("7 - Reboot (5 sec delay)");
            PCport.PrintLine("");
            PCport.Print("Please type a menu number and press enter: ");

            switch (PCport.Input())
            {
                case "1":
                    //WD.Write(false);
                    Config_network();
                    break;
                case "2":
                    //WD.Write(false);
                    Config_DHCP();
                    break;
                case "3":
                    //WD.Write(false);
                    Config_Server();
                    break;
                case "4":
                    //WD.Write(false);
                    Config_Save();
                    break;
                case "5":
                    //WD.Write(false);
                    Config_Restore();
                    break;
                case "6":
                    //WD.Write(false);
                    Config_Adv();
                    break;
                case "7":
                    //WD.Write(false);
                    LCD.ClearDisplay();
                    LCD.Write("Rebooting ...");
                    Thread.Sleep(3000);
                    Microsoft.SPOT.Hardware.PowerState.RebootDevice(false, 3000);
                    break;
                default:
                    //WD.Write(false);
                    Config();
                    break;
            }
            goto Config;

        }
        private bool Config_Adv()
        {
            Debug.Print("config: advanced");
        Config_Adv:
            WD.Write(true);
            Thread.Sleep(100);
            WD.Write(false);
            PCport.PrintClear();
            PCport.PrintLine("Advanced Menu");
            PCport.PrintLine("");
            PCport.PrintLine("Tc Offset: " + Tc_Offset);
            PCport.PrintLine("Rh Offset: " + Rh_offset);
            PCport.PrintLine("He Level Offset: " + He_Offset);
            PCport.PrintLine("He Pressure Offset: " + HeP_Offset);
            PCport.PrintLine("Water Flow Offset: " + Wf_Offset);
            PCport.PrintLine("Water Temp Offset: " + Wtf_Offset);
            PCport.PrintLine("FieldOn Detect Enabled: " + FieldOnDetect.ToString());
            PCport.PrintLine("Compressor Detect Enabled: " + CompressorDetect.ToString());
            PCport.PrintLine("Ping: " + pingable.ToString());
            PCport.PrintLine("");
            PCport.PrintLine("");
            PCport.PrintLine("1 - Tc Offset");
            PCport.PrintLine("2 - Rh Offset");
            PCport.PrintLine("3 - He Level Offset");
            PCport.PrintLine("4 - He Pressure Offset");
            PCport.PrintLine("5 - Water Flow Offset");
            PCport.PrintLine("6 - Water Temp Offset");
            PCport.PrintLine("7 - FieldOn Detect Enable/Disable");
            PCport.PrintLine("8 - Compressor Detect Enable/Disable");
            PCport.PrintLine("9 - Ping Enable/Disable");
            PCport.PrintLine("10 - Test Mode");
            PCport.PrintLine("11 - Main Menu");
            PCport.PrintLine("");
            PCport.Print("Please type a menu number and press enter: ");
            switch (PCport.Input())
            {
                case "1":
                    //WD.Write(false);
                    Config_Tc();
                    break;
                case "2":
                    //WD.Write(false);
                    Config_Rh();
                    break;
                case "3":
                    //WD.Write(false);
                    Config_He();
                    break;
                case "4":
                    //WD.Write(false);
                    Config_HeP();
                    break;
                case "5":
                    //WD.Write(false);
                    Config_Wf();
                    break;
                case "6":
                    //WD.Write(false);
                    Config_Wt();
                    break;
                case "7":
                    //WD.Write(false);
                    Config_FieldOn();
                    break;
                case "8":
                    //WD.Write(false);
                    Config_Compressor();
                    break;
                case "9":
                    //WD.Write(false);
                    Config_Ping();
                    break;
                case "10":
                    //WD.Write(false);
                    //Config_Rev();
                    TestMode();
                    break;
                case "11":
                    //WD.Write(false);
                    Config();
                    break;
                case "b00b135":
                    //WD.Write(false);
                    MakeSD();
                    Config_Adv();
                    break;
                default:
                    //WD.Write(false);
                    Config_Adv();
                    break;
            }
            goto Config_Adv;
        }
        private bool Config_FieldOn()
        {
            Debug.Print("config: field on");
            PCport.PrintClear();
            if (FieldOnDetect)
            {
                FieldOnDetect = false;
            }
            else
            {
                FieldOnDetect = true;
            }

            return true;
        }
        private bool Config_Compressor()
        {
            Debug.Print("config: compressor");
            PCport.PrintClear();
            if (CompressorDetect)
            {
                CompressorDetect = false;
            }
            else
            {
                CompressorDetect = true;
            }

            return true;
        }
        private bool Config_network()
        {
            Debug.Print("config: network");
            PCport.PrintClear();
            PCport.PrintLine("Network Settings Change");
            PCport.PrintLine("");
            PCport.Print("New IP: ");
            string ip = PCport.Input();
            PCport.PrintLine("");
            PCport.Print("New Subnet: ");
            string subnet = PCport.Input();
            PCport.PrintLine("");
            PCport.Print("New Gateway: ");
            string gw = PCport.Input();
            PCport.PrintLine("");
            PCport.PrintLine("");
            PCport.PrintLine("New Network Settings");
            PCport.PrintLine("");
            PCport.PrintLine("IP: " + ip);
            PCport.PrintLine("Subnet: " + subnet);
            PCport.PrintLine("Gateway: " + gw);
            PCport.PrintLine("");
            PCport.Print("Keep these settings (Y|N) ? ");
            if (PCport.Input().ToLower() == "y")
            {
                networkInterface.EnableStaticIP(ip, subnet, gw);
            }
            PCport.PrintLine("");
            PCport.PrintLine("");
            PCport.Print("Change DNS settings (Y|N) ? ");
            if (PCport.Input().ToLower() == "y")
            {
                PCport.PrintLine("");
                PCport.PrintLine("");
                PCport.Print("New Primary DNS: ");
                string pri = PCport.Input();
                PCport.PrintLine("");
                PCport.Print("New Secondary DNS: ");
                string sec = PCport.Input();
                PCport.PrintLine("");
                PCport.PrintLine("New DNS Settings");
                PCport.PrintLine("");
                PCport.PrintLine("Primary: " + pri);
                PCport.PrintLine("Secondary: " + sec);
                PCport.PrintLine("");
                PCport.Print("Keep these settings (Y|N) ? ");
                if (PCport.Input().ToLower() == "y")
                {
                    string[] addys = { pri, sec };
                    networkInterface.EnableStaticDns(addys);
                }
            }
            return true;

        }
        private bool Config_DHCP()
        {
            Debug.Print("config: dhcp");
            PCport.PrintClear();
            if (networkInterface.IsDhcpEnabled)
            {
                networkInterface.EnableStaticIP(networkInterface.IPAddress, networkInterface.SubnetMask, networkInterface.GatewayAddress);
            }
            else
            {
                networkInterface.EnableDhcp();
            }

            return true;
        }
        private bool Config_Tc()
        {
            Debug.Print("config: Tc");
            PCport.PrintClear();
            PCport.Print("Enter new Tc offset: ");
            string s = PCport.Input();
            Tc_Offset = Double.Parse(s);
            return true;
        }
        private bool Config_Ping()
        {
            Debug.Print("config: ping");
            PCport.PrintClear();
            if (pingable == true)
            {
                pingable = false;
            }
            else
            {
                pingable = true;
            }
            return true;
        }
        private bool Config_He()
        {
            Debug.Print("config: he");
            PCport.PrintClear();
            PCport.Print("Enter new He level offset: ");
            string s = PCport.Input();
            He_Offset = Double.Parse(s);
            return true;
        }
        private bool Config_HeP()
        {
            Debug.Print("config: hep");
            PCport.PrintClear();
            PCport.Print("Enter new He pressure offset: ");
            string s = PCport.Input();
            HeP_Offset = Double.Parse(s);
            return true;
        }
        private bool Config_Wf()
        {
            Debug.Print("config: wf");
            PCport.PrintClear();
            PCport.Print("Enter new water flow offset: ");
            string s = PCport.Input();
            Wf_Offset = Double.Parse(s);
            return true;
        }
        private bool Config_Wt()
        {
            Debug.Print("config: wt");
            PCport.PrintClear();
            PCport.Print("Enter new water temp offset: ");
            string s = PCport.Input();
            Wtf_Offset = Double.Parse(s);
            return true;
        }
        private bool Config_Rh()
        {
            Debug.Print("config: rh");
            PCport.PrintClear();
            PCport.Print("Enter new Rh offset: ");
            string s = PCport.Input();
            Rh_offset = Double.Parse(s);
            return true;
        }
        private bool Config_Server()
        {
            Debug.Print("config: server");
            PCport.PrintClear();
            PCport.Print("Enter new server address: ");
            string s = PCport.Input();
            Server = s;
            return true;
        }
        private bool Config_Save()
        {
            Debug.Print("config: save");

            FileStream file = File.Exists(@"\SD\config.cfg") ? new FileStream(@"\SD\config.cfg", FileMode.Create) : new FileStream(@"\SD\config.cfg", FileMode.Create);

            StreamWriter sw = new StreamWriter(file);
            sw.WriteLine(networkInterface.IPAddress.ToString());
            sw.WriteLine(networkInterface.SubnetMask.ToString());
            sw.WriteLine(networkInterface.GatewayAddress.ToString());
            sw.WriteLine(networkInterface.IsDhcpEnabled.ToString());
            sw.WriteLine(networkInterface.DnsAddresses[0].ToString());
            sw.WriteLine(networkInterface.DnsAddresses[1].ToString());
            sw.WriteLine(Tc_Offset.ToString());
            sw.WriteLine(Rh_offset.ToString());
            sw.WriteLine(Wtf_Offset.ToString());
            sw.WriteLine(He_Offset.ToString());
            sw.WriteLine(HeP_Offset.ToString());
            sw.WriteLine(Wf_Offset.ToString());
            sw.WriteLine(Server);
            sw.WriteLine(pingable.ToString());
            sw.WriteLine(FieldOnDetect.ToString());
            sw.WriteLine(CompressorDetect.ToString());
            sw.WriteLine("EOF");
            sw.Flush();
            sw.Close();

            file.Close();
            Debug.Print("Config Saved !");
            return true;
        }
        private bool Config_Restore()
        {
            Debug.Print("config: factory");
            PCport.PrintClear();
            PCport.Print("Reset to factory defaults (Y|N) ? ");
            if (PCport.Input().ToLower() == "y")
            {
                Factory_Defaults();
            }
            return true;
        }
        private bool Factory_Defaults()
        {
            networkInterface.EnableStaticIP("192.168.1.222", "255.255.255.0", "192.168.1.1");
            networkInterface.EnableStaticDns(new string[] { "8.8.4.4", "8.8.8.8" });
            networkInterface.EnableDhcp();
            Tc_Offset = -39.7;
            Rh_offset = 0;
            He_Offset = 0;
            HeP_Offset = 0;
            Wtf_Offset = 0;
            Wf_Offset = 0;
            pingable = true;
            FieldOnDetect = false;
            CompressorDetect = true;
            Server = defaultserver;
            return true;
        }
        private void TestMode()
        {
            Debug.Print("config: test mode");
            testmode = true;
            FirstRun = false;
            Scroll = 750;
            PCport.PrintClear();
            PCport.PrintLine("Test Mode Active");
            Thread.Sleep(3000);
            Run();
        }
        private string MAC()
        {
            NetworkInterface[] netIF = NetworkInterface.GetAllNetworkInterfaces();

            string macAddress = "";

            // Create a character array for hexidecimal conversion.
            const string hexChars = "0123456789ABCDEF";

            // Loop through the bytes.
            for (int b = 0; b < 6; b++)
            {
                // Grab the top 4 bits and append the hex equivalent to the return string.
                macAddress += hexChars[netIF[0].PhysicalAddress[b] >> 4];

                // Mask off the upper 4 bits to get the rest of it.
                macAddress += hexChars[netIF[0].PhysicalAddress[b] & 0x0F];

                // Add the dash only if the MAC address is not finished.
                if (b < 5) macAddress += "-";
            }

            this.mac = macAddress;
            return macAddress;
        }
        private bool Load_CFG()
        {
            Debug.Print("Load Config");
            string[] configfile = new string[16];
            try
            {
                using (StreamReader sr = new StreamReader(@"\SD\config.cfg"))
                {
                    string line;
                    int i = 0;
                    while ((line = sr.ReadLine()) != "EOF")
                    {
                        configfile[i] = line;
                        i = i + 1;
                    }
                    //Debug.Print("<<<< Config File >>>>");
                    foreach (string s in configfile)
                    {
                        //Debug.Print(s);
                        if (s == "")
                        {
                            throw new Exception("Config File Corrupt: blank line found");
                        }
                    }
                    //Debug.Print("<<<< Config File End>>>>");
                }


                Tc_Offset = Double.Parse(configfile[6]);
                Rh_offset = Double.Parse(configfile[7]);
                Wtf_Offset = double.Parse(configfile[8]);
                He_Offset = double.Parse(configfile[9]);
                HeP_Offset = double.Parse(configfile[10]);
                Wf_Offset = double.Parse(configfile[11]);
                Server = configfile[12];
                if (configfile[13].ToLower() == "true")
                {
                    pingable = true;
                }
                else
                {
                    pingable = false;
                }
                if (configfile[14].ToLower() == "true")
                {
                    FieldOnDetect = true;
                }
                else
                {
                    FieldOnDetect = false;
                }
                if (configfile[15].ToLower() == "true")
                {
                    CompressorDetect = true;
                }
                else
                {
                    CompressorDetect = false;
                }
                return true;
            }
            catch (Exception e)
            {
                Debug.Print("LoadCFG");
                Debug.Print(e.ToString());
                return false;
            }


        }
        public bool GetServerTime(int timeoffset)
        {
            if (GetServerTime2(timeoffset, "/monitor/mmtime.php"))
            {
                return true;
            }
            else
            {
                if (GetServerTime2(timeoffset, "http://" + Server + "/monitor/mmtime.php"))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public bool GetServerTime2(int timeoffset, string url)
        {
            //var url = "http://" + Server + "/monitor/mmtime.php";

            // If the device must go through a proxy server, then set proxy to the 
            // fqdn or ip of the proxy server.
            //string proxy = "";
            try
            {

                //DateTime networkDateTime = DateTime.Now;
                String html = GetWebPage(url, 80, Server);
                //Debug.Print(html);
                string reply = html;
                string[] servertime = new string[6]; //0(YYYY) 1(MM) 2(DD) 3(hh) 4(mm) 5(ss)
                int s = 0;
                s = reply.IndexOf("*");
                servertime[0] = reply.Substring((s + 1), 4).ToString();
                servertime[1] = reply.Substring((s + 6), 2).ToString();
                servertime[2] = reply.Substring((s + 9), 2).ToString();
                servertime[3] = reply.Substring((s + 12), 2).ToString();
                servertime[4] = reply.Substring((s + 15), 2).ToString();
                servertime[5] = reply.Substring((s + 18), 2).ToString();

                //foreach (string ser in servertime)
                //{
                //    Debug.Print(ser);
                //}

                DateTime networkDateTime = new DateTime(int.Parse(servertime[0]), int.Parse(servertime[1]), int.Parse(servertime[2]), int.Parse(servertime[3]), int.Parse(servertime[4]), int.Parse(servertime[5]));
                TimeSpan ts = new TimeSpan(0, timeoffset, 0, 0, 0);
                networkDateTime = networkDateTime.Subtract(ts);
                //Debug.Print(networkDateTime.ToString("G"));
                Utility.SetLocalTime(networkDateTime);
                return true;

            }
            catch (SocketException e)
            {
                Debug.Print("GetServerTime");
                Debug.Print(e.ToString());
                Microsoft.SPOT.Hardware.PowerState.RebootDevice(false, 3000);
                return false;
            }

        }
        public bool CheckMAC()
        {
            WD.Write(true);
            byte[] macfilebyte = new byte[17];
            int[] macfileint = new int[18];
            string[] macfile = new string[20];
            try
            {
                using (StreamReader sr = new StreamReader(@"\SD\inVision.cfg"))
                {
                    string line;
                    int i = 0;
                    while ((line = sr.ReadLine()) != null)
                    {
                        macfile[i] = line;
                        i = i + 1;
                    }
                    //Debug.Print("<<<< OI Vision File >>>>");

                    for (int s = 0; s <= macfile.Length - 1; s++)
                    {
                        //Debug.Print(macfile[s]);
                        if (macfile[s] != "")
                        {
                            if (macfile[s] != "EOF")
                            {
                                macfileint[s] = Convert.ToInt16(macfile[s]) / 16;
                                macfilebyte[s] = Convert.ToByte(macfileint[s].ToString());
                            }
                            else
                            {
                                break;
                            }

                        }
                        else
                        {
                            throw new Exception("OI Vision File Corrupt");//: blank line found");
                        }
                    }

                    //Debug.Print("<<<< OI Vision File End>>>>");
                }

                string mac = new string(System.Text.Encoding.UTF8.GetChars(macfilebyte));
                //Debug.Print("MAC Required: " + mac);
                string mac2 = MAC();
                //Debug.Print("MAC Found: " + mac2);
                if (mac.ToLower() != mac2.ToLower())
                {
                    throw new Exception("Mac check failed");
                }


            }
            catch (Exception e)
            {
                Debug.Print("Mac Check");
                Debug.Print(e.ToString());
                throw new Exception(e.ToString());
            }
            WD.Write(false);
            return true;
        }
        public bool MakeSD()
        {
            PCport.PrintLine("");
            PCport.PrintLine("");
            PCport.PrintLine("");
            PCport.PrintLine("Make SD");
            if (!VolumeExist())
            {
                PCport.PrintLine("No SD Card");
                Debug.Print("No SD Card");
                return false;
            }
            RemFiles();
            Debug.Print("SD Cleaned");
            PCport.PrintLine("SD Cleaded");
            if (MakeConfigFile())
            {
                Debug.Print("CFG Conplete");
                PCport.PrintLine("CFG Complete");
                if (MakeMacFile())
                {
                    Debug.Print("MAC Complete");
                    PCport.PrintLine("MAC Complete");
                }
            }
            Debug.Print("SD Complete");
            PCport.PrintLine("SD Complete");
            Thread.Sleep(1000);
            return true;
        }
        private bool VolumeExist()
        {
            VolumeInfo[] volumes = VolumeInfo.GetVolumes();
            foreach (VolumeInfo volumeInfo in volumes)
            {
                if (volumeInfo.Name.Equals("SD"))
                    return true;
            }

            return false;
        }
        public bool RemFiles()
        {
            try
            {
                string[] filePaths = Directory.GetFiles(@"\SD\");
                foreach (string filePath in filePaths)
                {
                    //Debug.Print(filePath.ToString());
                    try
                    {
                        File.Delete(filePath);
                    }
                    catch (Exception f)
                    {
                        Debug.Print("File Delete Error");
                        Debug.Print(f.ToString());
                    }
                }
            }
            catch (Exception e)
            {
                Debug.Print("RemFiles Error");
                Debug.Print(e.ToString());
            }
            return true;
        }
        public bool MakeMacFile()
        {
            string original_string = MAC();
            byte[] original_data = UTF8Encoding.UTF8.GetBytes(original_string);
            int[] new_data = new int[17];
            ushort a = 0;
            for (int i = 0; i <= original_data.Length - 1; i++)
            {
                a = Convert.ToUInt16(original_data[i].ToString());
                new_data[i] = a * 16;
            }
            try
            {
                FileStream file = new FileStream(@"\SD\inVision.cfg", FileMode.Create);
                StreamWriter sw = new StreamWriter(file);
                foreach (int l in new_data)
                {
                    sw.WriteLine(l.ToString());
                }
                sw.WriteLine("EOF");
                sw.Flush();
                sw.Close();
                file.Close();
                file.Dispose();
                sw.Dispose();
                Debug.Print("MAC Saved !");
            }
            catch (Exception e)
            {
                Debug.Print("MakeMacFile");
                Debug.Print(e.ToString());
            }
            return true;
        }
        public bool MakeConfigFile()
        {
            FileStream file = new FileStream(@"\SD\config.cfg", FileMode.Create);
            StreamWriter sw = new StreamWriter(file);
            string[] config = new string[17];
            config[0] = "192.168.1.100"; //IP
            config[1] = "255.255.255.0"; //Subnet
            config[2] = "192.168.1.1"; //Gateway
            config[3] = "True"; //DHCP enabled
            config[4] = "8.8.4.4"; //DNS Pri
            config[5] = "8.8.8.8"; //DNS Sec
            config[6] = "-39.7"; //tc offset (-41.7)
            config[7] = "0"; //rh offset
            config[8] = "0"; //wtf offset
            config[9] = "0"; //he offset
            config[10] = "0"; //hep offset
            config[11] = "0"; //wf offset
            config[12] = Server; //Server address
            config[13] = pingable.ToString(); //Server pingable
            config[14] = "True"; //FieldOn detect
            config[15] = "True"; //compressor detect
            config[16] = "EOF";
            foreach (string l in config)
            {
                //Debug.Print(l);
                sw.WriteLine(l.ToString());
            }
            sw.Flush();
            sw.Close();
            sw.Dispose();
            file.Close();
            file.Dispose();
            Debug.Print("Config Saved !");
            return true;
        }
        public bool Send()
        {


            if (testmode == true) { return true; }
            //var url = "http://" + Server + "/monitor/mm.php?mac=" + MAC() + "&date=" + DateTime.Now.ToString("s") + "&Tc=" + Tc.ToString("F") + "&Tf=" + Tf.ToString("F") + "&Rh=" + Rh.ToString("F")
            //    + "&He=" + He.ToString("F") + "&HeP=" + HeP.ToString("F") + "&Wf=" + Wf.ToString("F") + "&Wtc=" + Wtc.ToString("F") + "&Wtf=" + Wtf.ToString("F")
            //    + "&Q=" + FieldOn + "&CompOn=" + CompressorOn;
            var url = "/monitor/mm.php?mac=" + MAC() + "&date=" + DateTime.Now.ToString("s") + "&Tc=" + Tc.ToString("F") + "&Tf=" + Tf.ToString("F") + "&Rh=" + Rh.ToString("F")
               + "&He=" + He.ToString("F") + "&HeP=" + HeP.ToString("F") + "&Wf=" + Wf.ToString("F") + "&Wtc=" + Wtc.ToString("F") + "&Wtf=" + Wtf.ToString("F")
               + "&Q=" + FieldOn + "&CompOn=" + CompressorOn;
            try
            {
                Debug.Print("Sending Readings");
                //if (Ping())
                //{
                String html = GetWebPage(url, 80, Server);
                if (html.Equals("::nothing came back::"))
                {
                    Debug.Print("Trying to connect via second method");
                    html = DoSocketGet(url, Server);
                }
                //Debug.Print(html);
                string reply = html;
                int s;
                int e;
                s = reply.IndexOf("::");
                //Debug.Print(s.ToString());
                e = reply.IndexOf("::", (s + 1));
                //Debug.Print(e.ToString());
                if (reply.Substring((s + 2), (e - s) - 2).ToString() == "Inserted")
                {
                    d1 = DateTime.Now;
                    d2 = DateTime.Now.AddMinutes(DelayM);
                    //Debug.Print(reply.Substring((s + 2), (e - s) - 2).ToString());
                    return true;
                }
                else
                {
                    d1 = DateTime.Now;
                    d2 = DateTime.Now.AddMinutes(5);
                    //Debug.Print(reply.Substring((s + 2), (e - s) - 2).ToString());
                    //Debug.Print(reply);
                    FirstRun = true;
                    return false;
                }
                //}
                //else
                //{
                //    return false;
                //}
            }
            catch (SocketException se)
            {
                Debug.Print("SocketException when connecting to " + url + ".");
                Debug.Print(
                    "If your network uses IPSec, you may need enable the port manually");
                Debug.Print("Socket Error Code: " + se.ErrorCode.ToString());

                Debug.Print(se.ToString());
                //FirstRun = true;
                return false;
            }

        }
        private static String GetWebPage(String URL, Int32 port, String Dest)
        {
            // Creates a new web session
            HTTP_Client WebSession = new HTTP_Client(new IntegratedSocket(Dest, 80));

            // Requests the latest source
            HTTP_Client.HTTP_Response Response = WebSession.Get(URL);

            // Did we get the expected response? (a "200 OK")
            if (Response.ResponseCode != 200)
            {
                throw new ApplicationException("Unexpected HTTP response code: " + Response.ResponseCode.ToString());
                return "::nothing came back::";
            }

            // Gets the response as a string
            Debug.Print(Response.ToString());
            return Response.ToString();
        }
        private static Socket ConnectSocket(String server, Int32 port)
        {
            try
            {
                // Get server's IP address.
                IPHostEntry hostEntry = Dns.GetHostEntry(server);

                // Create socket and connect to the server's IP address and port
                Socket socket = new Socket(AddressFamily.InterNetwork,
                    SocketType.Stream, ProtocolType.Tcp);
                socket.ReceiveTimeout = 5000;
                socket.SendTimeout = 5000;
                uint freemem = Debug.GC(true);
                //socket.Connect(new IPEndPoint(hostEntry.AddressList[0], port));
                if (TryConnect(socket, new IPEndPoint(hostEntry.AddressList[0], port)) != true)
                {
                    NetworkInterface networkInterface = NetworkInterface.GetAllNetworkInterfaces()[0];
                    if (networkInterface.IsDhcpEnabled)
                    {
                        networkInterface.ReleaseDhcpLease();
                        networkInterface.RenewDhcpLease();
                    }
                    socket.Close();
                    return null;
                }
                return socket;
            }
            catch (Exception e)
            {
                Debug.Print("ConnectSocket -> ");
                Debug.Print(e.ToString());
                return null;
            }
        }
        public static bool TryConnect(Socket s, EndPoint ep)
        {
            bool connected = false;
            new Thread(delegate
            {
                try
                {
                    // s.SetSocketOption(SocketOptionLevel.Tcp, SocketOptionName.Linger, new byte[] { 0, 0, 0, 0 });                        
                    s.SetSocketOption(SocketOptionLevel.Tcp, SocketOptionName.NoDelay, true);
                    s.SetSocketOption(SocketOptionLevel.Tcp, SocketOptionName.KeepAlive, false);
                    s.SendTimeout = 2000;
                    s.ReceiveTimeout = 2000;
                    s.Connect(ep);

                    connected = true;
                }
                catch { }
            }).Start();
            int checks = 10;
            while (checks-- > 0 && connected == false) Thread.Sleep(100);
            if (connected == false)
            {
                throw new Exception("Failed to connect");
            }
            return connected;
        }
        public static string DoSocketGet(string url, string server)
        {
            //Set up variables and String to write to the server.
            Encoding ASCII = Encoding.UTF8;
            string Get = "GET " + url + " HTTP/1.1\r\nHost: " + server +
                         "\r\nConnection: Close\r\n\r\n";
            Byte[] ByteGet = ASCII.GetBytes(Get);
            Byte[] RecvBytes = new Byte[256];
            String strRetPage = null;


            // IPAddress and IPEndPoint represent the endpoint that will
            //   receive the request.
            // Get first IPAddress in list return by DNS.


            try
            {


                // Define those variables to be evaluated in the next for loop and 
                // then used to connect to the server. These variables are defined
                // outside the for loop to make them accessible there after.
                Socket s = null;
                IPEndPoint hostEndPoint;
                IPAddress hostAddress = null;
                int conPort = 80;

                // Get DNS host information.
                IPHostEntry hostInfo = Dns.GetHostEntry(server);
                // Get the DNS IP addresses associated with the host.
                IPAddress[] IPaddresses = hostInfo.AddressList;

                // Evaluate the socket and receiving host IPAddress and IPEndPoint. 
                for (int index = 0; index < IPaddresses.Length; index++)
                {
                    hostAddress = IPaddresses[index];
                    hostEndPoint = new IPEndPoint(hostAddress, conPort);


                    // Creates the Socket to send data over a TCP connection.
                    s = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);



                    // Connect to the host using its IPEndPoint.
                    s.Connect(hostEndPoint);

                    //if (!s.)
                    //{
                    //    // Connection failed, try next IPaddress.
                    //    strRetPage = "Unable to connect to host";
                    //    s = null;
                    //    continue;
                    //}

                    // Sent the GET request to the host.
                    s.Send(ByteGet, ByteGet.Length, 0);


                } // End of the for loop.      



                // Receive the host home page content and loop until all the data is received.
                Int32 bytes = s.Receive(RecvBytes, RecvBytes.Length, 0);
                strRetPage = "";
                //strRetPage = strRetPage + ASCII.GetChars(RecvBytes, 0, bytes);

                //while (bytes > 0)
                //{
                bytes = s.Receive(RecvBytes, RecvBytes.Length, 0);
                strRetPage = strRetPage + new String(Encoding.UTF8.GetChars(RecvBytes));
                //}


            } // End of the try block.

            catch (SocketException e)
            {
                Debug.Print("SocketException caught!!!");
                Debug.Print("Source : " + e.StackTrace);
                Debug.Print("Message : " + e.Message);
            }
            catch (ArgumentNullException e)
            {
                Debug.Print("ArgumentNullException caught!!!");
                Debug.Print("Source : " + e.StackTrace);
                Debug.Print("Message : " + e.Message);
            }
            catch (NullReferenceException e)
            {
                Debug.Print("NullReferenceException caught!!!");
                Debug.Print("Source : " + e.StackTrace);
                Debug.Print("Message : " + e.Message);
            }
            catch (Exception e)
            {
                Debug.Print("Exception caught!!!");
                Debug.Print("Source : " + e.StackTrace);
                Debug.Print("Message : " + e.Message);
            }
            //Debug.Print(strRetPage);
            return strRetPage;

        }
        public bool Ping() //this is pinging google
        {
            if (pingable == false) { return true; }
            //return true;
            //NetworkInterface[] netIF = NetworkInterface.GetAllNetworkInterfaces();

            RawSocketPing pingSocket = null;

            IPAddress remoteAddress = IPAddress.Parse("8.8.8.8");//Server.ToString());


            int dataSize = 512, ttlValue = 128, sendCount = 2;

            try
            {
                // Create a RawSocketPing class that wraps all the ping functionality
                pingSocket = new RawSocketPing(
                    ttlValue,
                    dataSize,
                    sendCount,
                    123
                    );

                // Set the destination address we want to ping
                pingSocket.PingAddress = remoteAddress;

                // Initialize the raw socket
                pingSocket.InitializeSocket();

                // Create the ICMP packets to send
                pingSocket.BuildPingPacket();

                // Actually send the ping request 
                bool success = pingSocket.DoPing();
                if (success)
                {
                    Debug.Print("Response from ping ");//to " + Server);
                    return true;
                }
                else
                {
                    //Debug.Print("Response from ping to " + Server + " - FAILED");
                    Debug.Print("No Response from ping");
                    return false;
                }
            }
            catch (SocketException err)
            {
                Debug.Print("Socket error occured: " + err.Message);
                return false;
            }
            finally
            {
                if (pingSocket != null)
                {
                    pingSocket.Close();
                }

            }

        }

        public void Testing()
        {
            string json;
            //JsonData jd = new JsonData(this.mac, 71.5, 75.2, 58.67, 4.03, 12.44, 58.65, true, false);
            //json = JsonSerializer.SerializeObject(jd);
            //Debug.Print(json);

            JsonHello jh = new JsonHello(this.mac, version + "_" + Rev);
            json = JsonSerializer.SerializeObject(jh);
            Debug.Print(json);

            //json = "{\"code\":202,\"error\":false,\"datetime\":\"2017-12-05T16:32:47+0000\",\"name\":\"High Field Shelbyville\",\"sysid\":\"MR2038\",\"enabled\":true,\"alarm\":false,\"critical\":true,\"freq\":60}";
            //Hashtable hashTable = JsonSerializer.DeserializeString(json) as Hashtable;
            //Debug.Print(hashTable["code"].ToString());

            IntegratedSocket socket = new IntegratedSocket("192.168.14.103", 8000);
            Debug.Print("Starting web request");

            HTTP_Client http = new HTTP_Client(socket);

            http.ContentType = "application/json";
            http.Headers = "api-key: 6548admin101317\r\n";
            HTTP_Client.HTTP_Response response = http.Post("/api/hello", json);
        
            if (response.ResponseCode != 202)
                throw new ApplicationException("Unexpected HTTP response code: " + response.ResponseCode.ToString());

            Debug.Print(response.ResponseBody.ToString());

            Hashtable hashTable = JsonSerializer.DeserializeString(response.ResponseBody.ToString()) as Hashtable;


            foreach (DictionaryEntry entry in (Hashtable)hashTable["settings"])
            {
                Debug.Print(entry.Key + ":" + entry.Value);
            }
            

            //Debug.Print(hashTable["settings"].ToString());
            Debug.Print("DONE");
            throw new Exception("JRD: Program End");
        }
    }
}
