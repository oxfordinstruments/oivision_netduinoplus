﻿/*
 * 2018-02-14 @ 12:24:00 -05:00
 * This the the current release code for the OiVision box controlled by a RDC 
 * Version 1.5.0.622
 */

using System;
using System.IO;
using System.Threading;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;
using SecretLabs.NETMF.Hardware.NetduinoPlus;
using System.Reflection;
using Microsoft.SPOT.Net.NetworkInformation;
using MCP320XTest;
using ElzeKool.io.sht11_io;
using ElzeKool.io;
using System.Collections;
using Json.NETMF;
using SerialPortLearn;
using Microsoft.SPOT.IO;


#if DEBUG
namespace System.Diagnostics
{
    public enum DebuggerBrowsableState
    {
        Never,
        Collapsed,
        RootHidden
    }
}
#warning Debugging Enabled
#endif

namespace OiVision2_NetduinoPlus
{
    public class Program
    {
#if DEBUG
        const bool debug_enable = true;
#else
        const bool debug_enable = false;
#endif

        /****** Code Debug Constants ******/
        const bool startGetMeasure = true;
        const bool startRebootWtch = true;
        const bool startSerialCmd = true;

        /****** Constants ******/
        const string RevDefault = "1.x"; //Default Board Revision for this software
        const double HeThreshold = 0.10;  // Value in V DC
        const double ADCRefVoltage = 5050;
        const int LCDBrightness = 255;

        /****** Variables ******/
        static string Rev = RevDefault;
        public bool firtsrun = true;
        static bool autoreboot = false;
        static string mac;
        static string version = "";
        ushort adc12bit;
        static bool testmode = true;
        static int scroll = 2000;
        static double Rh = 0;
        static double Dp = 0;
        static double DpD = 0;
        static double Tc = 0;
        static double Tf = 0;
        static bool q = false;
        static bool cmp = false;
        static int avg_cnt = 4;

        static bool monitor_he = true;
        static bool monitor_vp = true;
        static bool monitor_tf = true;
        static bool monitor_rh = true;
        static bool monitor_dp = true;
        static bool monitor_dpd = true;
        static bool monitor_wf = true;
        static bool monitor_wt = true;
        static bool monitor_q = true;
        static bool monitor_cmp = true;
        double convValue = 0.0;
        static double He_Offset = 0;
        static double Vp_Offset = 0;
        static double Wf_Offset = 0;
        static double Wtf_Offset = 0;
        static double Tc_Offset = -39.7;
        static double Rh_offset = 0.0;
        static double He = 111.11;
        double[] HeX = new double[4] { 0, 0, 0, 0 };
        static double Vp = 0;
        double[] VpX = new double[4] { 0, 0, 0, 0 };
        static double Wf = 0;
        double[] WfX = new double[4] { 0, 0, 0, 0 };
        static double Wtc = 0;
        static double Wtf = 0;
        double[] WtfX = new double[4] { 0, 0, 0, 0 };
        static string LCDMsg1 = "";
        static string LCDMsg2 = "";
        static string LCDErr1 = "";
        static string LCDErr2 = "";
        static string LCDUpld = "";
        static Hashtable errors = new Hashtable();

        /****** IO ******/
        MCP320X mcp320xA = new MCP320X(2, Pins.GPIO_PIN_D9);
        MCP320X mcp320xB = new MCP320X(2, Pins.GPIO_PIN_D10);
        private SHT11_GPIO_IOProvider SHT1x_IO;
        public SensirionSHT11 SHT1x;
        static LXIT.Hardware.SerLCD LCD = new LXIT.Hardware.SerLCD(SerialPorts.COM1);
        static OutputPort ProgPin = new OutputPort(Pins.GPIO_PIN_D4, true);
        InputPort FieldOnPin = new InputPort(Pins.GPIO_PIN_D5, true, Port.ResistorMode.Disabled); //Change accordingly for FieldOn Switch
        InputPort CompressorOnPin = new InputPort(Pins.GPIO_PIN_D6, true, Port.ResistorMode.Disabled); //Change Accordingly for Compressor On
        static SerialPortHelper PCport = new SerialPortHelper(SerialPorts.COM2, 57600);
        OutputPort MLED = new OutputPort(Pins.GPIO_PIN_A5, true);
        OutputPort WD = new OutputPort(Pins.GPIO_PIN_D7, false);

        /****** Threads ******/
        public static Thread _GetMeasurments;
        public static Thread _RebootWatch;
        public static Thread _WatchDogConfig;
        public static Thread _Run;
        public static Thread _SerData;

        public static void Main()
        {
#if DEBUG
            Thread.Sleep(3000);
#endif

            var MainProg = new Program();
            _Run = new Thread(MainProg.Run);
            _SerData = new Thread(SerData);
            debug("Main: Starting Main");
            MAC();

            Assembly asm = Assembly.GetExecutingAssembly();
            AssemblyName assemblyName = asm.GetName();
            version = assemblyName.Version.ToString();

            _SerData.Start();

            if (!LoadConfigFile())
            {
                SaveConfigFile();
            }

            _Run.Start();

            // Create IO provider for temperature sensor
            MainProg.SHT1x_IO = new SHT11_GPIO_IOProvider(Pins.GPIO_PIN_A0, Pins.GPIO_PIN_A1);

            // Create SHT11 Interface with the IO provider we've just created
            MainProg.SHT1x = new SensirionSHT11(MainProg.SHT1x_IO);
        }
        public static void debug(string obj)
        {
            if (debug_enable)
            {
                Debug.Print(obj);
            }
        }
        public static void lcdMsg(string line1, string line2, int sleep = 2000, bool reboot = false)
        {
            LCD.ClearDisplay();
            LCD.MoveHome();
            LCD.Write(line1);
            LCD.SetCursor(1, 2);
            LCD.Write(line2);
            Thread.Sleep(sleep);
            if (reboot)
            {
                LCD.ClearDisplay();
                LCD.MoveHome();
                LCD.Write("Rebooting...");                
                Thread.Sleep(2000);
                PowerState.RebootDevice(false, 3000);
            }
        }
        private static string MAC()
        {
            NetworkInterface[] netIF = NetworkInterface.GetAllNetworkInterfaces();

            string macAddress = "";

            // Create a character array for hexidecimal conversion.
            const string hexChars = "0123456789ABCDEF";

            // Loop through the bytes.
            for (int b = 0; b < 6; b++)
            {
                // Grab the top 4 bits and append the hex equivalent to the return string.
                macAddress += hexChars[netIF[0].PhysicalAddress[b] >> 4];

                // Mask off the upper 4 bits to get the rest of it.
                macAddress += hexChars[netIF[0].PhysicalAddress[b] & 0x0F];

                // Add the dash only if the MAC address is not finished.
                if (b < 5) macAddress += "-";
            }

            mac = macAddress;
            return macAddress;
        }
        private static void RebootWatch()
        {
            debug("Starting RebootWatch Thread");
            Thread.Sleep(30000);
            DateTime d1 = DateTime.Now;
            Random r1 = new Random();
            int r2 = r1.Next(16);
            while (true)
            {
                debug("<<<<<<<<<<<<<<<<<<< Uptime >>>>>>>>>>>>>>>>>>>>" + PowerState.Uptime.ToString());
                Thread.Sleep(10000);
                d1 = DateTime.Now;
                if (PowerState.Uptime.Hours >= 1 && PowerState.Uptime.Seconds >= 1 && d1.Hour == 06 && d1.Minute >= r2 && d1.Second >= r2)
                {
                    debug("Reboot !!!!!!");
                    PowerState.RebootDevice(false, 3000);
                }
                if (PowerState.Uptime.Days >= 1 && PowerState.Uptime.Seconds >= 1 && d1.Hour == 06 && d1.Minute >= r2 && d1.Second >= r2)
                {
                    debug("Reboot !!!!!!");
                    PowerState.RebootDevice(false, 3000);
                }
                if (PowerState.Uptime.Days >= 2)
                {
                    PowerState.RebootDevice(false, 3000);
                }

            }
        }
        private void WatchDogConfig()
        {
            while (true)
            {
                WD.Write(true);
                Thread.Sleep(10);
                WD.Write(false);
                Thread.Sleep(500);
            }
        }
        private void pingWatchDog()
        {
            WD.Write(true);
            Thread.Sleep(10);
            WD.Write(false);
        }
        public void Run()
        {
            debug("Starting Run Thread");
            string tmpQC;

            LCD.Open();
            Setup();

            if (startRebootWtch && autoreboot)
            {
                debug("Starting Reboot Watch");
                _RebootWatch = new Thread(delegate () { RebootWatch(); });
                _RebootWatch.Start();
            }


            if (startGetMeasure)
            {
                debug("Starting Get Measurements");
                _GetMeasurments = new Thread(delegate () { GetMeasurements(); });
                _GetMeasurments.Start();
            }

            while (true)
            {
                WD.Write(true);
                Thread.Sleep(10);
                WD.Write(false);
                if (startGetMeasure)
                {
                    if (_GetMeasurments.IsAlive == false)
                    {
                        _GetMeasurments.Start();
                    }
                }

                MeasureRoomTemperatureAndRh();
                CheckMagnetOutputs();
                if (testmode == true)
                {
                    lcdMsg("Test Mode", "No RDC Connection", scroll);
                }
                else
                {
                    lcdMsg("   RDC  Connected   ", "", scroll);
                }
                pingWatchDog();

                if (testmode != true)
                {
                    lcdMsg(DateTime.Now.ToString("MMMM d, yyyy"), DateTime.Now.ToString("HH:mm:ss") + " UTC", scroll);
                }

                if (LCDMsg1 != "" || LCDMsg2 != "")
                {
                    if(LCDMsg1 != "" && LCDMsg2 == "")
                    {
                        lcdMsg(LCDMsg1, "", scroll*2);
                    }
                    else if (LCDMsg2 != "" && LCDMsg1 == "")
                    {
                        lcdMsg("", LCDMsg2, scroll*2);
                    }
                    else
                    {
                        lcdMsg(LCDMsg1, LCDMsg2, scroll*2);
                    }
                }

                if (LCDErr1 != "" || LCDErr2 != "")
                {
                    if (LCDErr1 != "" && LCDErr2 == "")
                    {
                        lcdMsg(LCDErr1, "", scroll*2);
                    }
                    else if (LCDErr2 != "" && LCDErr1 == "")
                    {
                        lcdMsg("", LCDErr2, scroll*2);
                    }
                    else
                    {
                        lcdMsg(LCDErr1, LCDErr2, scroll*2);
                    }
                }
                if (LCDUpld != "")
                {
                    lcdMsg("Last Upload", LCDUpld, scroll*2);
                }

                pingWatchDog();

                if (monitor_tf)
                {
                    LCD.ClearDisplay();
                    LCD.Write("Room Temperature");
                    LCD.SetCursor(1, 2);
                    LCD.Write(Tc.ToString("F") + " ");
                    LCD.Write(0);
                    LCD.Write("  " + Tf.ToString("F") + " ");
                    LCD.Write(1);
                    Thread.Sleep(scroll);
                }

                if (monitor_rh)
                {
                    lcdMsg("Room Humidity", Rh.ToString("F") + " %RH", scroll);
                }

                if (monitor_dp)
                {
                    LCD.ClearDisplay();
                    LCD.Write("Dew Point");
                    LCD.SetCursor(1, 2);
                    LCD.Write(Dp.ToString("F") + " ");
                    LCD.Write(1);
                    Thread.Sleep(scroll);
                }

                if (monitor_dpd)
                {
                    LCD.ClearDisplay();
                    LCD.Write("Dew Point Diff");
                    LCD.SetCursor(1, 2);
                    LCD.Write(DpD.ToString("F") + " ");
                    LCD.Write(1);
                    Thread.Sleep(scroll);
                }


                pingWatchDog();

                if (monitor_he)
                {
                    if (He != 111.11)
                    {
                        lcdMsg("He Level", He.ToString("F") + " %", scroll);
                    }
                    else
                    {
                        lcdMsg("He Level", "N/A", scroll);
                    }
                }

                if (monitor_vp)
                {
                    if (Vp >= -1.0)
                    {
                        lcdMsg("Vessel Pressure", Vp.ToString("F") + " PSIG", scroll);
                    }
                    else
                    {
                        lcdMsg("Vessel Pressure", "N/A", scroll);
                    }
                }

                if (monitor_wf)
                {
                    if (Wf > 0.0)
                    {
                        lcdMsg("Water Flow", Wf.ToString("F") + " GPM", scroll);
                    }
                    else
                    {
                        lcdMsg("Water Flow", "N/A", scroll);
                    }
                }
                
                pingWatchDog();

                if (monitor_wt)
                {
                    LCD.ClearDisplay();
                    LCD.Write("Water Temp");
                    LCD.SetCursor(1, 2);
                    if (Wtf >= 10.0)
                    {
                        LCD.Write(Wtc.ToString("F"));
                        LCD.Write(0);
                        LCD.Write("  " + Wtf.ToString("F"));
                        LCD.Write(1);
                    }
                    else
                    {
                        LCD.Write("N/A");
                    }
                    Thread.Sleep(scroll);
                }
                

                if (monitor_q == true)
                {
                    if (q == true)
                    {
                        tmpQC = "ON";
                    }
                    else
                    {
                        tmpQC = "OFF";
                    }
                    lcdMsg("Magnet Field", tmpQC, scroll);
                }


                if (monitor_cmp)
                {
                    if (cmp)
                    {
                        tmpQC = "ON";
                    }
                    else
                    {
                        tmpQC = "OFF";
                    }
                    lcdMsg("Compressor", tmpQC, scroll);
                }
                
                pingWatchDog();
                
            }

        }
        public bool Setup()
        {
            pingWatchDog();
            LCD.Write(new byte[] { 0xFE, 0x40 });
            LCD.Write("     OiVision        Oxford Instruments "); //Clears LCD Startup Screen
            Thread.Sleep(100);
            LCD.Write(new byte[] { 0xFE, 0x4E, 0x00, 0x08, 0x14, 0x08, 0x03, 0x04, 0x04, 0x03, 0x00 }); //Degree C symbol
            Thread.Sleep(100);
            LCD.Write(new byte[] { 0xFE, 0x4E, 0x01, 0x08, 0x14, 0x08, 0x07, 0x04, 0x06, 0x04, 0x00 }); //Degree F symbol
            Thread.Sleep(100);

            lcdMsg("OiVision", "Ver: " + version + "_" + Rev, scroll);

            lcdMsg("Starting Up...", "", 100);

            LCD.SetCursor(1, 2);
            LCD.Write("SHT Init");
            Thread.Sleep(300);
            // Soft-Reset the SHT11
            if (SHT1x.SoftReset())
            {
                // Softreset returns True on error
                lcdMsg("SHT Start Error", "", 10000, true);
                throw new Exception("Error while resetting SHT11");
            }
            pingWatchDog();
            // Set Temperature and Humidity to less acurate 12/8 bit
            if (SHT1x.WriteStatusRegister(SensirionSHT11.SHT11Settings.LessAcurate))
            {
                // WriteRegister returns True on error
                lcdMsg("SHT 12 Bit Error", "", 10000, true);
                throw new Exception("Error while writing status register SHT11");
            }
            LCD.SetCursor(1, 2);
            LCD.Write("SHT Check   ");
            MeasureRoomTemperatureAndRh();
            Thread.Sleep(500);
            LCD.SetCursor(1, 2);

            
            return true;
        }
        public static void SerData()
        {
            string instr = "";
            while (true)
            {
                PCport.PrintLine("CMD?");
                switch (PCport.Input().ToLower())
                {
                    case "h":
                    case "help":
                        cmdAccepted();
                        getHelp();
                        break;
                    case "d":
                    case "data":
                        cmdAccepted();
                        getData();
                        break;
                    case "t":
                    case "time":
                        cmdAccepted();
                        getTime();
                        break;
                    case "&t":
                    case "&time":
                        cmdAccepted();
                        instr = PCport.Input();
                        setTime(instr);
                        break;
                    case "&l1":
                    case "&line1":
                        cmdAccepted();
                        instr = PCport.Input();
                        setLCDMsg(1, instr);
                        break;
                    case "&l2":
                    case "&line2":
                        cmdAccepted();
                        instr = PCport.Input();
                        setLCDMsg(2, instr);
                        break;
                    case "&e1":
                    case "&err1":
                        cmdAccepted();
                        instr = PCport.Input();
                        setLCDErr(1, instr);
                        break;
                    case "&e2":
                    case "&err2":
                        cmdAccepted();
                        instr = PCport.Input();
                        setLCDErr(2, instr);
                        break;
                    case "f":
                    case "eclear":
                        cmdAccepted();
                        clearLCDErr();
                        break;
                    case "&u":
                    case "&upload":
                        cmdAccepted();
                        instr = PCport.Input();
                        setLCDUpld(instr);
                        break;
                    case "n":
                    case "nclear":
                        cmdAccepted();
                        clearLCDUpld();
                        break;
                    case "o":
                    case "offsets":
                        cmdAccepted();
                        getOffsets();
                        break;
                    case "&o":
                    case "&offsets":
                        cmdAccepted();
                        instr = PCport.Input();
                        setOffsets(instr);
                        break;
                    case "m":
                    case "monitor":
                        cmdAccepted();
                        getMonitor();
                        break;
                    case "&m":
                    case "&monitor":
                        cmdAccepted();
                        instr = PCport.Input();
                        setMonitor(instr);
                        break;
                    case "c":
                    case "clear":
                        cmdAccepted();
                        clearLCDMsg();
                        break;
                    case "r":
                    case "reboot":
                        cmdAccepted();
                        lcdMsg("Rebooting!", "", scroll, true);
                        break;
                    case "x":
                        testmode = false;
                        cmdAccepted();
                        break;
                    case "z":
                        cmdAccepted();
                        streamData();                        
                        break;
                    case "a":
                        autoreboot = true;
                        cmdAccepted();
                        break;
                    case "!a":
                        autoreboot = false;
                        cmdAccepted();
                        break;
                    case "b":
                    case "box":
                        cmdAccepted();
                        getBoxParam();
                        break;
                    case "&f":
                    case "&factory":
                        cmdAccepted();
                        setFactory();
                        break;
                    case "&i":
                    case "&rev":
                        cmdAccepted();
                        instr = PCport.Input();
                        setRev(instr);
                        break;
                    case "e":
                    case "error":
                        cmdAccepted();
                        getErrors();
                        break;
                    default:
                        PCport.PrintLine("UNKNOWN CMD");
                        break;
                }
            }
        }
        public static void getHelp()
        {
            PCport.PrintLine(" Commands Help");
            PCport.PrintLine(" h   help     : Show this help menu.");
            PCport.PrintLine(" d   data     : Print the current readings.");
            PCport.PrintLine(" t   time     : Get current UTC date/time.");
            PCport.PrintLine(" &t  &time    : Set date/time. Example: &time[RETURN]{\"time\":\"2000-01-30T16:48:23\"}[RETURN]");
            PCport.PrintLine(" o   offsets  : Get current offsets.");
            PCport.PrintLine(" &o  &offsets : Set offsets. Example: &o[RETURN]{\"he\":0.0,\"vp\":0.0,\"wtf\":0.0,\"wf\":0.0,\"tc\":-39.7,\"rh\":0.0}");
            PCport.PrintLine(" m   monitor  : Get current monitor params.");
            PCport.PrintLine(" &m  &monitor : Set monitor params. Example: &v[RETURN]{\"he\":true,\"vp\":true,\"wtf\":true,\"wf\":true,\"tc\":true,\"rh\":true,\"q\":false,\"cmp\":true}");
            PCport.PrintLine(" b   box      : Get current box params.");
            PCport.PrintLine(" $l1 &line1   : Set LCD Message Line 1. Example: &l1[RETURN]Hello World[RETURN]");
            PCport.PrintLine(" &l2 &line2   : Set LCD Message Line 2. Example: &l2[RETURN]Hello World[RETURN]");
            PCport.PrintLine(" c   clear    : Clear LCD Message");
            PCport.PrintLine(" r   reboot   : Reboot now");
            PCport.PrintLine(" &i  &rev     : Set box board revision. Example: &i[RETURN]{\"rev\":\"1.7\"}");
            PCport.PrintLine(" &f  &factory : Set factory defaults");
            PCport.PrintLine(" e   errors   : Get error log");
            PCport.PrintLine(" $e1 &err1    : Set LCD Error Message Line 1. Example: &e1[RETURN]Hello World[RETURN]");
            PCport.PrintLine(" &e2 &err2    : Set LCD Error Message Line 2. Example: &e2[RETURN]Hello World[RETURN]");
            PCport.PrintLine(" f   eclear   : Clear LCD Error Message");
            PCport.PrintLine(" &u  &upload  : Set LCD Upload Message. Example: &u[RETURN]Hello World[RETURN]");
            PCport.PrintLine(" n   nclear   : Clear LCD Upload Message");
            PCport.PrintLine(" x            : RDC Connected.");
            PCport.PrintLine(" z            : Stream Data Mode. [SPACE][RETURN] to exit");
            PCport.PrintLine(" a            : Enable auto reboot");
            PCport.PrintLine(" !a           : Disable auto reboot (default)");
            
        }
        public static void cmdAccepted()
        {
            PCport.PrintLine("*");
        }
        public static void getData()
        {
            Hashtable hashtable = new Hashtable();
            hashtable.Add("he", He);
            hashtable.Add("vp", Vp);
            hashtable.Add("tf", Tf);
            hashtable.Add("tc", Tc);
            hashtable.Add("rh", Rh);
            hashtable.Add("dp", Dp);
            hashtable.Add("dpd", DpD);
            hashtable.Add("wf", Wf);
            hashtable.Add("wtf", Wtf);
            hashtable.Add("wtc", Wtc);
            hashtable.Add("comp", cmp);
            hashtable.Add("q", q);
            string json = JsonSerializer.SerializeObject(hashtable);
            PCport.PrintLine(json);

        }
        public static void getMac()
        {
            Hashtable hashtable = new Hashtable();
            hashtable.Add("mac", mac);
            string json = JsonSerializer.SerializeObject(hashtable);
            PCport.PrintLine(json);
        }
        public static void getTime()
        {
            Hashtable hashtable = new Hashtable();
            hashtable.Add("time", DateTime.Now);
            string json = JsonSerializer.SerializeObject(hashtable);
            PCport.PrintLine(json);
        }
        public static void setTime(string json)
        {
            try
            {
                Hashtable ht = JsonSerializer.DeserializeString(json) as Hashtable;
                if (ht.Count == 0)
                {
                    PCport.PrintLine("INVALID");
                    return;
                }

                foreach (DictionaryEntry de in ht)
                {
                    switch ((string)de.Key)
                    {
                        case "time":
                        case "date":
                            DateTime dt = DateTimeExtensions.FromIso8601((string)de.Value);
                            Utility.SetLocalTime(dt);
                            cmdAccepted();
                            break;
                    }
                }
            }
            catch (NullReferenceException nre)
            {
                PCport.PrintLine("INVALID");
                errors.Add(DateTime.Now, "SetTime: " + nre.Message);
                return;
            }
            catch (Exception e)
            {
                PCport.PrintLine("INVALID");
                errors.Add(DateTime.Now, "SetTime: " + e.Message);
                return;
            }
            cmdAccepted();

        }
        public static void getOffsets()
        {
            Hashtable hashtable = new Hashtable();
            hashtable.Add("he", He_Offset);
            hashtable.Add("vp", Vp_Offset);
            hashtable.Add("wtf", Wtf_Offset);
            hashtable.Add("wf", Wf_Offset);
            hashtable.Add("tc", Tc_Offset);
            hashtable.Add("rh", Rh_offset);
            string json = JsonSerializer.SerializeObject(hashtable);
            PCport.PrintLine(json);
        }
        public static void setOffsets(string json)
        {
            try { 
                Hashtable ht = JsonSerializer.DeserializeString(json) as Hashtable;
                if(ht.Count == 0)
                {
                    PCport.PrintLine("INVALID");
                    return;
                }
                foreach (DictionaryEntry de in ht)
                {
                    switch ((string)de.Key)
                    {
                        case "he":
                            He_Offset = (double)de.Value;
                            break;
                        case "vp":
                            Vp_Offset = (double)de.Value;
                            break;
                        case "wtf":
                            Wtf_Offset = (double)de.Value;
                            break;
                        case "wf":
                            Wf_Offset = (double)de.Value;
                            break;
                        case "tc":
                            Tc_Offset = (double)de.Value;
                            break;
                        case "rh":
                            Rh_offset = (double)de.Value;
                            break;
                    }
                }
            }
            catch(NullReferenceException nre)
            {
                PCport.PrintLine("INVALID");
                errors.Add(DateTime.Now, "SetOffsets 1: " + nre.Message);
                return;
            }
            catch(InvalidCastException ice)
            {
                PCport.PrintLine("INVALID");
                errors.Add(DateTime.Now, "SetOffsets 2: " + ice.Message);
                return;
            }
            
            cmdAccepted();
        }
        public static void getMonitor()
        {
            Hashtable hashtable = new Hashtable();
            hashtable.Add("he", monitor_he);
            hashtable.Add("vp", monitor_vp);
            hashtable.Add("tf", monitor_tf);
            hashtable.Add("rh", monitor_rh);
            hashtable.Add("dp", monitor_dp);
            hashtable.Add("dpd", monitor_dpd);
            hashtable.Add("wf", monitor_wf);
            hashtable.Add("wt", monitor_wt);
            hashtable.Add("q", monitor_q);
            hashtable.Add("cmp", monitor_cmp);
            string json = JsonSerializer.SerializeObject(hashtable);
            PCport.PrintLine(json);
        }
        public static void setMonitor(string json)
        {
            try
            {
                Hashtable ht = JsonSerializer.DeserializeString(json) as Hashtable;
                if (ht.Count == 0)
                {
                    PCport.PrintLine("INVALID");
                    return;
                }

                foreach (DictionaryEntry de in ht)
                {
                    switch ((string)de.Key)
                    {
                        case "he":
                            monitor_he = (bool)de.Value;
                            break;
                        case "vp":
                            monitor_vp = (bool)de.Value;
                            break;
                        case "tf":
                            monitor_tf = (bool)de.Value;
                            break;
                        case "rh":
                            monitor_rh = (bool)de.Value;
                            break;
                        case "dp":
                            monitor_dp = (bool)de.Value;
                            break;
                        case "dpd":
                            monitor_dpd = (bool)de.Value;
                            break;
                        case "wf":
                            monitor_wf = (bool)de.Value;
                            break;
                        case "wt":
                            monitor_wt = (bool)de.Value;
                            break;
                        case "q":
                            monitor_q = (bool)de.Value;
                            break;
                        case "cmp":
                            monitor_cmp = (bool)de.Value;
                            break;
                        default:
                            break;
                    }
                }
            }
            catch(NullReferenceException nre)
            {
                PCport.PrintLine("INVALID");
                errors.Add(DateTime.Now, "SetMonitor: " + nre.Message);
                return;
            }
            cmdAccepted();
        }
        public static void setLCDMsg(int line, string str)
        {
            if (line == 1)
            {
                LCDMsg1 = str;
            }
            else
            {
                LCDMsg2 = str;
            }
            cmdAccepted();
        }
        public static void setLCDErr(int line, string str)
        {
            if (line == 1)
            {
                LCDErr1 = str;
            }
            else
            {
                LCDErr2 = str;
            }
            cmdAccepted();
        }
        public static void setLCDUpld(string str)
        {
            LCDUpld = str;
            cmdAccepted();
        }
        public static void clearLCDMsg()
        {
            LCDMsg1 = "";
            LCDMsg2 = "";
            cmdAccepted();
        }
        public static void clearLCDErr()
        {
            LCDErr1 = "";
            LCDErr2 = "";
            cmdAccepted();
        }
        public static void clearLCDUpld()
        {
            LCDUpld = "";
            cmdAccepted();
        }
        public static void streamData()
        {
            while(true)
            {
                if (PCport.ReadLine() != "")
                {
                    return;
                }
                getData();
                Thread.Sleep(1000);
            }
        }
        public static void getBoxParam()
        {
            Hashtable hashtable = new Hashtable();
            hashtable.Add("autoreboot", autoreboot);
            hashtable.Add("version", version);
            hashtable.Add("revision", Rev);
            hashtable.Add("testmode", testmode);
            hashtable.Add("mac", mac);
            hashtable.Add("date", DateTime.Now);
            hashtable.Add("uptime", PowerState.Uptime.ToString());
            string json = JsonSerializer.SerializeObject(hashtable);
            PCport.PrintLine(json);
        }
        public static void getErrors()
        {
            string json = JsonSerializer.SerializeObject(errors);
            PCport.PrintLine(json);
        }
        public static void setFactory()
        {
            setOffsets("{\"he\":0.0,\"vp\":0.0,\"wtf\":0.0,\"wf\":0.0,\"tc\":-39.7,\"rh\":0.0}");
            setMonitor("{\"q\":true,\"cmp\":false}");
            clearLCDMsg();
            if (VolumeExist())
            {
                RemFiles();
            }
            cmdAccepted();
        }
        public static void setRev(string json)
        {

            try
            {
                Hashtable ht = JsonSerializer.DeserializeString(json) as Hashtable;
                if (ht.Count == 0)
                {
                    PCport.PrintLine("INVALID");
                    return;
                }

                foreach (DictionaryEntry de in ht)
                {
                    switch ((string)de.Key)
                    {
                        case "rev":
                        case "revision":
                            Rev = (string)de.Value;
                            SaveConfigFile();
                            cmdAccepted();
                            break;
                    }
                }
            }
            catch (NullReferenceException nre)
            {
                PCport.PrintLine("INVALID");
                errors.Add(DateTime.Now, "SetRev: " + nre.Message);
                return;
            }
            catch (Exception e)
            {
                PCport.PrintLine("INVALID");
                errors.Add(DateTime.Now, "SetRev: " + e.Message);
                return;
            }
            cmdAccepted();            
        }
        private void GetMeasurements()
        {
            debug("Starting Measurment Thread");
            while (true)
            {
                MLED.Write(true);
                //Water Flow
                adc12bit = mcp320xA.ReadADC(0, true, 50);
                //debug("Water Flow ADC Value: " + adc12bit);
                // convert from 12 bit value to V reading... assume we're using 5.0V as the supply voltage.
                convValue = (ADCRefVoltage * adc12bit / 4096) / 1000;
                //debug("Water Flow (Vdc): " + convValue);
                //Polynomial math goes here.  example: 5v=100gpm and 1v=0gpm
                convValue = (convValue * 0.7511274019160) + 0.6031050243629;
                //debug("Water Flow Reading Actual: " + convValue);
                if (firtsrun)
                {
                    WfX[0] = convValue + Wf_Offset;
                    WfX[1] = convValue + Wf_Offset;
                    WfX[2] = convValue + Wf_Offset;
                    WfX[3] = convValue + Wf_Offset;
                    if ((convValue + Wf_Offset) >= 0.90)
                    {
                        Wf = convValue + Wf_Offset;
                    }
                    else
                    {
                        Wf = 0.0;
                    }
                }
                else
                {
                    WfX[0] = WfX[1];
                    WfX[1] = WfX[2];
                    WfX[2] = WfX[3];
                    WfX[3] = convValue + Wf_Offset;
                    if ((convValue + Wf_Offset) >= 0.90)
                    {
                        Wf = (WfX[0] + WfX[1] + WfX[2] + WfX[3]) / 4;
                    }
                    else
                    {
                        Wf = 0.0;
                    }

                }
                //debug("Water Flow Display: " + Wf.ToString("F"));

                //Water Temp
                adc12bit = mcp320xA.ReadADC(1, true, 50);
                //debug("Water Temp ADC Value: " + adc12bit);
                // convert from 12 bit value to V reading... assume we're using 5.0V as the supply voltage.
                convValue = (ADCRefVoltage * adc12bit / 4096) / 1000;
                //debug("Water Temp (Vdc): " + convValue);
                //Polynomial math goes here.  example: 5v=100 deg c and 1v=0 deg c
                //convValue = (convValue * 25.4423) - 25.1447;//old poly
                convValue = (convValue * 20.08413234737) + 0.1576700508843;
                //debug("Water Temp F Reading Actual: " + convValue);
                if (firtsrun)
                {
                    WtfX[0] = convValue + Wtf_Offset;
                    WtfX[1] = convValue + Wtf_Offset;
                    WtfX[2] = convValue + Wtf_Offset;
                    WtfX[3] = convValue + Wtf_Offset;
                    Wtf = convValue + Wtf_Offset;
                }
                else
                {
                    WtfX[0] = WtfX[1];
                    WtfX[1] = WtfX[2];
                    WtfX[2] = WtfX[3];
                    WtfX[3] = convValue + Wtf_Offset;
                    Wtf = (WtfX[0] + WtfX[1] + WtfX[2] + WtfX[3]) / 4;
                }
                Wtc = ((Wtf - 32) * 5) / 9;

                //debug("Water Temp F Display: " + Wtf.ToString("F"));
                //debug("Water Temp C Display: " + Wtc.ToString("F"));

                //He Pressure
                adc12bit = mcp320xB.ReadADC(1, true, 50);
                //debug("He Pressure ADC Value: " + adc12bit);
                // convert from 12 bit value to V reading... assume we're using 5.0V as the supply voltage.
                convValue = (ADCRefVoltage * adc12bit / 4096) / 1000;
                //debug("He Pressure(Vdc): " + convValue);
                //Polynomial math goes here.  example: 5v=100 deg c and 1v=0 deg c
                //convValue = (convValue * 3.636523949299) + -3.596646745677;
                convValue = (convValue * 3.627402531871) + -3.633238886333;
                //debug("He Pressure Reading Actual: " + convValue);
                if (firtsrun)
                {
                    VpX[0] = convValue + Vp_Offset;
                    VpX[1] = convValue + Vp_Offset;
                    VpX[2] = convValue + Vp_Offset;
                    VpX[3] = convValue + Vp_Offset;
                    Vp = convValue + Vp_Offset;
                }
                else
                {
                    VpX[0] = VpX[1];
                    VpX[1] = VpX[2];
                    VpX[2] = VpX[3];
                    VpX[3] = convValue + Vp_Offset;
                    Vp = (VpX[0] + VpX[1] + VpX[2] + VpX[3]) / 4;
                }
                //debug("He Pressure Display: " + HeP.ToString("F"));

                //He %
                adc12bit = mcp320xB.ReadADC(0, true, 100);
                //debug("He Level ADC Value: " + adc12bit);
                // convert from 12 bit value to V reading... assume we're using 5.0V as the supply voltage.
                convValue = (ADCRefVoltage * adc12bit / 4096) / 1000;
                //debug("He Level (Vdc): " + convValue);
                if (convValue >= HeThreshold)
                {
                    //Polynomial math goes here.  example: 5v=0% and 1v=100%
                    //convValue = (((convValue * 100) * -0.145849) + 99.94723); // old poly
                    //convValue = (convValue * -27.19246423233) + 99.97433368139; //old poly kinda works
                    convValue = (convValue * -21.08302807570) + 99.95558514737;
                    //debug("He Reading Actual: " + (convValue - He_Offset) + "  -Updated");
                    if (firtsrun)
                    {
                        HeX[0] = convValue + He_Offset;
                        HeX[1] = convValue + He_Offset;
                        He = convValue + He_Offset;
                    }
                    else
                    {
                        HeX[0] = HeX[1];
                        HeX[1] = convValue + He_Offset;
                        He = (HeX[0] + HeX[1]) / 2;
                    }
                    //He = convValue + He_Offset;
                    //He_Save();
                }
                //else
                //{
                //    debug("He Reading Actual: " + (convValue + He_Offset));
                //}
                //debug("He Reading Display: " + He.ToString("F"));



                debug("HE: " + He.ToString("F") + " VP: " + Vp.ToString("F") + " WTf: " + Wtf.ToString("F") + " WTc: " + Wtc.ToString("F") + " WF: " + Wf.ToString("F") + "");


                MLED.Write(false);
                Thread.Sleep(300);
                MLED.Write(true);
                Thread.Sleep(200);
                MLED.Write(false);
                Thread.Sleep(2000);
                firtsrun = false;
            }
        }
        private void MeasureRoomTemperatureAndRh()
        {
            try
            {
                SHT1x.readTemp = 0.0;
            }
            catch(NullReferenceException nre)
            {
                debug(nre.Message);
                debug("Trying again");
                errors.Add(DateTime.Now, "MeasureRhTemp: " + nre.Message);
                MeasureRoomTemperatureAndRh();
            }
            

            // Read Temperature with SHT11 VDD = +/- 3.5V and in Celcius
            var temperature = SHT1x.ReadTemperature(SensirionSHT11.SHT11VDD_Voltages.VDD_3_5V,
                SensirionSHT11.SHT11TemperatureUnits.Celcius);
            temperature = temperature + Tc_Offset;

            double f = ((temperature * 9) / 5) + 32;

            // Read Humidity with SHT11 VDD = +/- 3.5V 
            var humiditiy = SHT1x.ReadRelativeHumidity(SensirionSHT11.SHT11VDD_Voltages.VDD_3_5V);
            humiditiy = humiditiy + Rh_offset;
            Tc = temperature;
            Tf = f;
            Rh = humiditiy;
            //debug("Room Temp Deg C: " + Tc.ToString("F"));
            //debug("Room Temp Deg F: " + Tf.ToString("F"));
            //debug("Room Humidity: " + Rh.ToString("F"));

            //Calculate the dew point in F
            var dew = (System.Math.Pow((Rh / 100), 0.125)) * (112 + 0.9 * Tc) + (0.1 * Tc) - 112;
            Dp = ((dew * 9) / 5) + 32;

            //Calculate the Dew point diff
            DpD = f - Dp;

            debug("Tc: " + Tc.ToString("F") +  " Tf: " + Tf.ToString("F") + " Rh: " + Rh.ToString("F") + " Dp: " + Dp.ToString("F") + " DpDiff: " + DpD.ToString("F"));

        }
        private void CheckMagnetOutputs()
        {
            if (monitor_q == true) {
                q = FieldOnPin.Read();
            } else {
                q = true;
            }

            if (monitor_cmp == true) { 
                cmp = CompressorOnPin.Read();
            } else {
                cmp = true;
            }
            //debug("Field On: " + FieldOn.ToString());
            //debug("Compressor On: " + CompressorOn.ToString());

            debug("Field: " + q.ToString() + " Compressor: " + cmp.ToString());
        }
        public static bool SaveConfigFile()
        {
            try
            {
                if (VolumeExist())
                {
                    FileStream file = File.Exists(@"\SD\config2.cfg") ? new FileStream(@"\SD\config2.cfg", FileMode.Create) : new FileStream(@"\SD\config2.cfg", FileMode.Create);

                    StreamWriter sw = new StreamWriter(file);
                    sw.WriteLine(Rev);

                    sw.WriteLine("EOF");
                    sw.Flush();
                    sw.Close();
                    file.Close();
                    debug("Config Saved !");
                    return true;
                }else
                {
                    throw new Exception("No SD Card");
                }
            }
            catch(System.IO.IOException sie)
            {
                debug(sie.Message);
                errors.Add(DateTime.Now, "SaveConfigFile: " + sie.Message);
                return false;
            }
           
        }
        private static bool LoadConfigFile()
        {
            debug("Load Config");
            string[] configfile = new string[1];
            try
            {
                using (StreamReader sr = new StreamReader(@"\SD\config2.cfg"))
                {
                    string line;
                    int i = 0;
                    while ((line = sr.ReadLine()) != "EOF")
                    {
                        configfile[i] = line;
                        i = i + 1;
                    }
                    foreach (string s in configfile)
                    {
                        if (s == "")
                        {
                            debug("Config File Corrupt: blank line found");
                            lcdMsg("Config Corrupt", "Using Defaults", scroll);
                            throw new Exception("Config File Corrupt: blank line found");
                        }
                    }
                }

                
                Rev = configfile[0];

                debug("Config Loaded");
                return true;
            }
            catch (Exception e)
            {
                debug("Loaded Config Error");
                debug(e.ToString());
                errors.Add(DateTime.Now, "LoadConfigFiles: " + e.Message);
                return false;
            }


        }
        public static bool RemFiles()
        {
            try
            {
                string[] filePaths = Directory.GetFiles(@"\SD\");
                foreach (string filePath in filePaths)
                {
                    try
                    {
                        File.Delete(filePath);
                    }
                    catch (Exception f)
                    {
                        debug("File Delete Error");
                        debug(f.ToString());
                        errors.Add(DateTime.Now, "RemFiles 1: " + f.Message);
                    }
                }
            }
            catch (Exception e)
            {
                debug("RemFiles Error");
                debug(e.ToString());
                errors.Add(DateTime.Now, "RemFiles 2: " + e.Message);
            }
            return true;
        }
        private static bool VolumeExist()
        {
            try
            {
                VolumeInfo[] volumes = VolumeInfo.GetVolumes();
                foreach (VolumeInfo volumeInfo in volumes)
                {
                    if (volumeInfo.Name.Equals("SD"))
                        return true;
                }
                return false;
            }
            catch(Exception e)
            {
                debug(e.ToString());
                errors.Add(DateTime.Now, "VolumeExist: " + e.Message);
                return false;
            }
        }
    }
}
