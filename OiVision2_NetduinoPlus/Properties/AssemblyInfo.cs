﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("OiVision2_NetduinoPlus")]
[assembly: AssemblyDescription("Designed by Evotodi")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Oxford Instruments Healthcare")]
[assembly: AssemblyProduct("OiVision2_NetduinoPlus")]
[assembly: AssemblyCopyright("Copyright ©  2018")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("1.5.0.622")]
[assembly: AssemblyFileVersion("1.5.0.622")]
