//using System;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;
using SecretLabs.NETMF.Hardware;
using SecretLabs.NETMF.Hardware.NetduinoPlus;

namespace MCP320XTest
{
    partial class MCP320X
    {

        // ===================
        //  Public properties
        // ===================
        public ushort NumChannels { get; set; }
        public uint ClockRateKHz { get; set; }
        public Cpu.Pin ChipSelectPin { get; set; }

        // Use digital pin 10 for the chip select signal as a default
        //protected const Cpu.Pin defaultChipSelectPin = Pins.GPIO_PIN_D10;
        // Datasheet indicates a clock rate of 2MHz for 5V, 1MHz for 2.7V (also works at 3.3).
        protected const uint defaultClockRateKHz = 2000;

        // ====================
        //   Instance methods
        // ====================
        public MCP320X(ushort numChannels) { SetConfig(numChannels, defaultClockRateKHz, Pins.GPIO_PIN_D10); }
        public MCP320X(ushort numChannels, Cpu.Pin chipSelectPin) { SetConfig(numChannels, defaultClockRateKHz, chipSelectPin); }
        public MCP320X(ushort numChannels, uint clockRateKHz) { SetConfig(numChannels, clockRateKHz, Pins.GPIO_PIN_D10); }
        public MCP320X(ushort numChannels, Cpu.Pin chipSelectPin, uint clockRateKHz) { SetConfig(numChannels, clockRateKHz, chipSelectPin); }

        // ============================
        //   Setting / getting config
        // ============================
        protected void SetConfig(ushort numChannels, uint clockRateKHz, Cpu.Pin chipSelectPin)
        {
            ChipSelectPin = chipSelectPin;
            ClockRateKHz = clockRateKHz;
            NumChannels = numChannels;
        }
        protected SPI.Configuration CurrentConfig()
        {
            return new SPI.Configuration(
               ChipSelectPin,
               false,
               0,
               0,
               false,
               true,
               ClockRateKHz,
               SPI.SPI_module.SPI1);
        }

        // ===================
        //   ReadADC methods
        // ===================
        public ushort ReadADC(ushort channel, bool singleEnded = true, uint numSamples = 1)
        {
            ushort retVal = 0;
            uint adcTotal = 0;

            // Make sure user passed a valid channel value
            if (channel > (NumChannels - 1)) { channel = (ushort)(NumChannels - 1); }

            byte[] writeArray;
            byte[] readArray = new byte[2];

            // Create the command byte for the MCP3208. Datasheet indicates:
            //   * minimum 5 "zeros"
            //   * start bit (1) 
            //   * indicator for single-ended (1) or differential (0)
            //   * 3 bits indicating channel to read (0 thru 7, or 000 thru 111)
            //   * remainder padded out with zeros (they don't really matter).
            //
            // Fitting this into a 2-byte message, the format would be:
            //   ex) 00000110 11000000   for reading single-ended channel 3 (011)   

            ushort commandBits;
    
            if (singleEnded)
            {
                // Start with a default 11000000 value
                commandBits = 0xC0;
            }
            else
            {
                // Start with a default 10000000 value
                commandBits = 0x80;
            }

            if (NumChannels > 2)
            {
                // command for a MCP3204 or MCP3208
                // Shift the channel bits over 3 places and OR them to the command byte
                commandBits |= (ushort)(channel << 3);
                // shift them 3 more bits
                commandBits = (ushort)(commandBits << 3);
            }
            else
            {
                // command for MCP3202 - slightly different from the MCP3204/8 chips.
                // Shift the channel bits over 5 bits and OR them to the command byte
                commandBits |= (ushort)(channel << 5);
                // shift them 1 more bit
                commandBits = (ushort)(commandBits << 1);
            }

            
            // break out the integer value into bytes
            byte lowByte = (byte)(commandBits & 0xFF);
            byte highByte = (byte)(commandBits >> 8);

            // combine our two bites into the proper byte array
            writeArray = new byte[] { highByte, lowByte };

            // Send the command and read the response.
            SPI.Configuration config = CurrentConfig();
            using (SPI spi = new SPI(config))
            {
                // Take a reading as many times as the user specified and then average them
                for (int i = 0; i < numSamples; i++)
                {
                    spi.WriteRead(writeArray, readArray, 1);
                    adcTotal += (uint)(((readArray[0] & 0x0F) << 8) + readArray[1]);
                }
                // get our average value...
                retVal = (ushort)(adcTotal / numSamples);
                spi.Dispose();
            }

            // Ignore the first 4 bits of the first word - the first 3 are "1" and the 4th is NULL, 
            //  per the datasheet.

            return retVal;

        }
    }
}
