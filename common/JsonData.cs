﻿using System.Collections;

public class JsonData
{
    public string mac { get; set; }
    public string mfg {
        get
        {
            return _mfg;
        }
        set
        {
            _mfg = value;
        }
    }
    public Hashtable data
    {
        get
        {
            return _data;
        }
        set
        {
            _data = value;
        }
    }

    private Hashtable _data;
    private string _mfg = "GE";

    private double tf { get; set; }
    private double rh { get; set; }
    private double he { get; set; }
    private double vp { get; set; }
    private double wf { get; set; }
    private double wt { get; set; }
    private bool q { get; set; }
    private bool cmp { get; set; }

    public JsonData(string mac, double tf = 0, double rh = 0, double he = 0, double vp = 0, double wf = 0, double wt = 0, bool q = true, bool cmp = true)
    {
        this.mac = mac;

        this.tf = tf;
        this.rh = rh;
        this.he = he;
        this.vp = vp;
        this.wf = wf;
        this.wt = wt;
        this.q = q;
        this.cmp = cmp;

        Hashtable data = new Hashtable();
        data.Add("tf", this.tf);
        data.Add("rh", this.rh);
        data.Add("he", this.he);
        data.Add("vp", this.vp);
        data.Add("wf", this.wf);
        data.Add("wt", this.wt);
        data.Add("q", this.q);
        data.Add("cmp", this.cmp);

        this._data = data;
    }
}

public class JsonHello
{
    public string mac { get; set; }
    public string ver { get; set; }

    public JsonHello(string mac, string ver)
    {
        this.mac = mac;
        this.ver = ver;
    }
}
