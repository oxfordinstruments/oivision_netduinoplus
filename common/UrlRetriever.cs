using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
//using System.Threading;
//using Microsoft.SPOT;

namespace Netduino.Network
{
    public class UrlRetriever
    {
        public static String GetWebPage(String Proxy, String URL, Int32 port)
        {
            const Int32 c_microsecondsPerSecond = 1000000;
            string host = GetHostFromURL(URL);
            string server = Proxy.Length > 0 ? Proxy : host;

            using (Socket serverSocket = ConnectSocket(server, port))
            {
                String request = "GET " + URL + " HTTP/1.1\r\nHost: " + host + "\r\nConnection: Close\r\n\r\n";
                Byte[] bytesToSend = Encoding.UTF8.GetBytes(request);
                serverSocket.Send(bytesToSend, bytesToSend.Length, 0);

                Byte[] buffer = new Byte[1024];

                String page = String.Empty;

                // Wait up to 30 seconds for initial data to be available.  Throws 
                // an exception if the connection is closed with no data sent.
                DateTime timeoutAt = DateTime.Now.AddSeconds(30);
                while (serverSocket.Available == 0 && DateTime.Now < timeoutAt)
                {
                    System.Threading.Thread.Sleep(100);
                }

                // Poll for data until 30-second timeout.  Returns true for data and 
                // connection closed.
                while (serverSocket.Poll(30 * c_microsecondsPerSecond,
                    SelectMode.SelectRead))
                {
                    // If there are 0 bytes in the buffer, then the connection is 
                    // closed, or we have timed out.
                    if (serverSocket.Available == 0)
                        break;

                    // Zero all bytes in the re-usable buffer.
                    Array.Clear(buffer, 0, buffer.Length);

                    // Read a buffer-sized HTML chunk.
                    Int32 bytesRead = serverSocket.Receive(buffer);

                    // Append the chunk to the string.
                    page = page + new String(Encoding.UTF8.GetChars(buffer));
                }

                return page;
            }
        }

        private static Socket ConnectSocket(String server, Int32 port)
        {
            IPHostEntry hostEntry = Dns.GetHostEntry(server);

            Socket socket = new Socket(AddressFamily.InterNetwork,
                SocketType.Stream, ProtocolType.Tcp);
            socket.Connect(new IPEndPoint(hostEntry.AddressList[0], port));
            return socket;
        }

        private static String GetHostFromURL(string URL)
        {
            int start = URL.IndexOf("://");
            int end = start >= 0 ? URL.IndexOf('/', start + 3) : URL.IndexOf('/');
            if (start >= 0)
            {
                start += 3;

                if (end >= 0)
                    return URL.Substring(start, end - start);

                else
                    return URL.Substring(start);
            }

            if (end >= 0)
                return URL.Substring(0, end + 1);
            return URL;
        }
    }
}
